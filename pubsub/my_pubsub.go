// +build !solution

package pubsub

import (
	"context"
	"errors"
	uuid2 "github.com/gofrs/uuid"
	"sync"
)

var _ Subscription = (*MySubscription)(nil)

type MySubscription struct {
	uuid         uuid2.UUID
	handler      MsgHandler
	currentIndex int
	lastIndex    int
	parentTopic  *MyTopic
}

func (s *MySubscription) Unsubscribe() {
	s.parentTopic.removeHandler(s.uuid)
}

var _ PubSub = (*MyPubSub)(nil)

type MyPubSub struct {
	sync.Mutex
	topics       map[string]*MyTopic
	closed       bool
	closeContext context.Context
}

type MyTopic struct {
	sync.Cond
	subscribers  map[uuid2.UUID]*MySubscription
	queue        []interface{}
	close        chan int
	parentPubSub *MyPubSub
}

func NewPubSub() PubSub {
	return &MyPubSub{
		topics:       make(map[string]*MyTopic),
		closed:       false,
		Mutex:        sync.Mutex{},
		closeContext: context.Background(),
	}
}

func (p *MyPubSub) Subscribe(subj string, cb MsgHandler) (Subscription, error) {
	if p.closed {
		return nil, errors.New("PubSub was cloased")
	}
	p.Lock()
	topic, ok := p.topics[subj]
	if !ok {
		topic = &MyTopic{
			Cond:         *sync.NewCond(&sync.Mutex{}),
			subscribers:  make(map[uuid2.UUID]*MySubscription),
			queue:        make([]interface{}, 0),
			parentPubSub: p,
		}
	}
	p.topics[subj] = topic
	newHandler := topic.addHandler(cb)
	p.Unlock()
	return newHandler, nil
}

func (p *MyPubSub) Publish(subj string, msg interface{}) error {
	if p.closed {
		return errors.New("PubSub was cloased")
	}
	p.Lock()
	defer p.Unlock()
	topic, ok := p.topics[subj]
	if !ok {
		return errors.New("NO SUBJECT")
	}
	topic.addMessage(msg)
	return nil
}

func (p *MyPubSub) Close(ctx context.Context) error {
	if p.closed {
		return errors.New("PubSub was closed")
	}
	p.closeContext = ctx
	p.closed = true
	for _, topic := range p.topics {
		topic.closeTopic()
	}
	return nil
}

func (t *MyTopic) addMessage(msg interface{}) {
	t.L.Lock()
	t.queue = append(t.queue, msg)
	t.L.Unlock()
	t.Broadcast()
}

func (t *MyTopic) closeTopic() {
	t.L.Lock()
	lastItem := len(t.queue)
	for _, subscription := range t.subscribers {
		subscription.lastIndex = lastItem
	}
	t.L.Unlock()
	t.Broadcast()
}

func (t *MyTopic) addHandler(handler MsgHandler) *MySubscription {
	uuid, _ := uuid2.DefaultGenerator.NewV1()
	newSubscription := &MySubscription{
		uuid:         uuid,
		handler:      handler,
		parentTopic:  t,
		currentIndex: 0,
		lastIndex:    -1,
	}
	t.L.Lock()
	newSubscription.currentIndex = len(t.queue)
	t.subscribers[uuid] = newSubscription
	t.L.Unlock()
	go newSubscription.handleMessages()
	return newSubscription
}

func (t *MyTopic) removeHandler(uuid uuid2.UUID) {
	t.L.Lock()
	defer t.L.Unlock()
	subscription, ok := t.subscribers[uuid]
	if !ok {
		return
	}
	subscription.lastIndex = len(t.queue)
	delete(t.subscribers, uuid)
}

func (s *MySubscription) handleMessages() {
	for {
		select {
		case <-s.parentTopic.parentPubSub.closeContext.Done():
			return
		default:
			msg, err := s.getNextMsg()
			if err != nil {
				switch err.Error() {
				case "FINISH":
					return
				case "REPEAT":
					continue
				}
			}
			s.handler(msg)
		}
	}
}

func (s *MySubscription) getNextMsg() (interface{}, error) {
	s.parentTopic.L.Lock()
	defer s.parentTopic.L.Unlock()
	queueLen := len(s.parentTopic.queue)
	if s.currentIndex == s.lastIndex {
		return "", errors.New("FINISH")
	}
	if queueLen <= s.currentIndex {
		s.parentTopic.Wait()
		return "", errors.New("REPEAT")
	}
	s.currentIndex++
	return s.parentTopic.queue[s.currentIndex-1], nil
}

// +build !solution

package jsonlist

import (
	"encoding/json"
	"io"
	"reflect"
	"strings"
)

func Marshal(w io.Writer, slice interface{}) error {
	sType := reflect.TypeOf(slice)
	sliceType := reflect.ValueOf(slice)
	if sType.Kind() != reflect.Slice {
		return &json.UnsupportedTypeError{Type: sType}
	}
	builder := strings.Builder{}
	if sliceType.Kind() == reflect.Slice {
		for i := 0; i < sliceType.Len(); i++ {
			index := sliceType.Index(i)
			marshal, err := json.Marshal(index.Interface())
			if err != nil {
				return err
			}
			builder.Write(marshal)
			builder.WriteRune(' ')
		}
	}
	res := strings.TrimSpace(builder.String())
	_, _ = w.Write([]byte(res))
	return nil
}

func Unmarshal(r io.Reader, slice interface{}) error {
	sType := reflect.TypeOf(slice)
	if sType.Kind() != reflect.Ptr {
		return &json.UnsupportedTypeError{Type: sType}
	}
	sValue := reflect.ValueOf(slice).Elem()
	if sValue.Kind() == reflect.Slice {
		decoder := json.NewDecoder(r)
		for {
			if !decoder.More() {
				break
			}
			arrayElemntType := sValue.Type()
			//elem := sType.Elem()
			newValue := reflect.New(arrayElemntType.Elem())
			err := decoder.Decode(newValue.Interface())
			if err != nil {
				return err
			}
			sValue.Set(reflect.Append(sValue, newValue.Elem()))

		}
	}
	return nil
}

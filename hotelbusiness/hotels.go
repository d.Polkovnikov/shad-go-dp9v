// +build !solution

package hotelbusiness

import "sort"

type Guest struct {
	CheckInDate  int
	CheckOutDate int
}

type Load struct {
	StartDate  int
	GuestCount int
}

type FinalLoad struct {
	changes []Load
}

func ComputeLoad(guests []Guest) []Load {
	preLoad := FinalLoad{}
	for _, guest := range guests {
		preLoad.changes = append(preLoad.changes, Load{StartDate: guest.CheckInDate, GuestCount: 1})
		preLoad.changes = append(preLoad.changes, Load{StartDate: guest.CheckOutDate, GuestCount: -1})
	}
	sort.Sort(preLoad)
	result := make([]Load, 0)
	currentIndex := -1
	prevCount := -1
	for _, change := range preLoad.changes {
		if currentIndex == -1 {
			result = append(result, change)
			currentIndex++
		} else {
			if result[currentIndex].StartDate != change.StartDate {
				if prevCount == result[currentIndex].GuestCount {
					result[currentIndex].StartDate = change.StartDate
				} else {
					result = append(result, Load{
						StartDate:  change.StartDate,
						GuestCount: result[currentIndex].GuestCount,
					})
					prevCount = result[currentIndex].GuestCount
					currentIndex++
				}
			}
			result[currentIndex].GuestCount += change.GuestCount
		}
	}

	return result
}

func (g FinalLoad) Len() int {
	return len(g.changes)
}

func (g FinalLoad) Less(i, j int) bool {
	return g.changes[i].StartDate < g.changes[j].StartDate
}

func (g FinalLoad) Swap(i, j int) {
	g.changes[i], g.changes[j] = g.changes[j], g.changes[i]
}

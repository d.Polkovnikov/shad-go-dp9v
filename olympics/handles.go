package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
)

var athletes = make([]Athlete, 0)

func loadWinners(path string) {
	jsonFile, _ := os.Open(path)
	byteValue, _ := ioutil.ReadAll(jsonFile)
	_ = json.Unmarshal(byteValue, &athletes)
	collectByName(&athletes)
	collectBySport(&athletes)
	collectByYear(&athletes)
}

func getAthleteInfo(w http.ResponseWriter, r *http.Request) {
	keys, ok := r.URL.Query()["name"]
	if !ok || len(keys[0]) < 1 {
		http.NotFound(w, r)
		return
	}
	name := keys[0]
	res, ok := AthletesDataByName[name]
	if !ok {
		http.NotFound(w, r)
		return
	}
	response, _ := json.Marshal(res)
	_, _ = fmt.Fprintf(w, "%s", response)
}

func getBestAthletes(w http.ResponseWriter, r *http.Request) {
	sporsKeys, ok := r.URL.Query()["sport"]
	if !ok || len(sporsKeys[0]) < 1 {
		http.NotFound(w, r)
		return
	}
	limit := 3
	limitKeys, ok := r.URL.Query()["limit"]
	if ok && len(limitKeys[0]) > 0 {
		var err error
		limit, err = strconv.Atoi(limitKeys[0])
		if err != nil {
			http.Error(w, "error", http.StatusBadRequest)
			return
		}
	}
	data, ok := AthletesDataBySport[sporsKeys[0]]
	if !ok {
		http.NotFound(w, r)
		return
	}
	if limit != -1 || limit > len(data) {
		data = data[:limit]
	}
	response, _ := json.Marshal(data)
	_, _ = fmt.Fprintf(w, "%s", response)
}

func getBestCounties(w http.ResponseWriter, r *http.Request) {
	yearKeys, ok := r.URL.Query()["year"]
	if !ok || len(yearKeys[0]) < 1 {
		http.NotFound(w, r)
		return
	}
	year, _ := strconv.Atoi(yearKeys[0])
	limit := 3
	limitKeys, ok := r.URL.Query()["limit"]
	if ok && len(limitKeys[0]) > 0 {
		var err error
		limit, err = strconv.Atoi(limitKeys[0])
		if err != nil {
			http.Error(w, "error", http.StatusBadRequest)
			return
		}
	}
	data, ok := MedalsByYear[year]

	if !ok {
		http.NotFound(w, r)
		return
	}
	if limit > -1 && limit < len(data) {
		data = data[:limit]
	}
	response, _ := json.Marshal(data)
	_, _ = fmt.Fprintf(w, "%s", response)
}

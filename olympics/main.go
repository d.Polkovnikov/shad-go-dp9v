// +build !solution

package main

import (
	"flag"
	"log"
	"net/http"
)

const (
	AthleteInfoURL  = "/athlete-info"
	TopAthletesURL  = "/top-athletes-in-sport"
	TopCountriesURL = "/top-countries-in-year"
)

var (
	port     = flag.String("port", "8000", "")
	datapPth = flag.String("data", "", "")
)

func main() {
	flag.Parse()
	loadWinners(*datapPth)
	x := RegexpHandler{}
	x.HandleFunc(AthleteInfoURL, "GET", getAthleteInfo)
	x.HandleFunc(TopAthletesURL, "GET", getBestAthletes)
	x.HandleFunc(TopCountriesURL, "GET", getBestCounties)
	log.Fatal(http.ListenAndServe("localhost:"+*port, &x))
}

package main

import (
	"sort"
	"strconv"
	"strings"
)

type Athlete struct {
	Athlete string
	Age     int
	Country string
	Year    int
	Date    string
	Sport   string
	Gold    int
	Silver  int
	Bronze  int
	Total   int
}

type AthleteShortData struct {
	Name    string
	Country string
}

type Medal struct {
	Gold   int `json:"gold"`
	Silver int `json:"silver"`
	Bronze int `json:"bronze"`
	Total  int `json:"total"`
}

type AthleteData struct {
	Athlete      string           `json:"athlete"`
	Country      string           `json:"country"`
	Medals       Medal            `json:"medals"`
	MedalsByYear map[string]Medal `json:"medals_by_year"`
}

type MedalByCountry struct {
	Country string `json:"country"`
	Gold    int    `json:"gold"`
	Silver  int    `json:"silver"`
	Bronze  int    `json:"bronze"`
	Total   int    `json:"total"`
}

type ByMedal []MedalByCountry

type Athletes []AthleteData

var AthletesDataByName = make(map[string]AthleteData)
var AthletesDataBySport = make(map[string]Athletes)
var MedalsByYear = make(map[int]ByMedal)

func collectByName(athletes *[]Athlete) {
	athletesByName := make(map[string][]Athlete)
	for _, athlete := range *athletes {
		_, ok := athletesByName[athlete.Athlete]
		if !ok {
			athletesByName[athlete.Athlete] = make([]Athlete, 0)
		}
		athletesByName[athlete.Athlete] = append(athletesByName[athlete.Athlete], athlete)
	}
	for athlete, key := range athletesByName {
		AthletesDataByName[athlete] = createAthleteData(key)
	}
}

func collectBySport(athletes *[]Athlete) {
	athletesBySport := make(map[string][]Athlete)
	for _, athlete := range *athletes {
		_, ok := athletesBySport[athlete.Sport]
		if !ok {
			athletesBySport[athlete.Sport] = make([]Athlete, 0)
		}
		athletesBySport[athlete.Sport] = append(athletesBySport[athlete.Sport], athlete)
	}
	for sport, athletes := range athletesBySport {
		AthletesDataBySport[sport] = make(Athletes, 0)

		athletesByData := make(map[AthleteShortData][]Athlete)
		for _, ath := range athletes {
			shortData := AthleteShortData{
				Name:    ath.Athlete,
				Country: ath.Country,
			}
			_, ok := athletesByData[shortData]
			if !ok {
				athletesByData[shortData] = make([]Athlete, 0)
			}
			athletesByData[shortData] = append(athletesByData[shortData], ath)
		}
		for _, athletes := range athletesByData {
			athleteData := createAthleteData(athletes)
			AthletesDataBySport[sport] = append(AthletesDataBySport[sport], athleteData)
		}
		sort.Sort(AthletesDataBySport[sport])
	}
}

func collectByYear(athletes *[]Athlete) {
	athletesByYear := make(map[int][]Athlete)
	for _, athlete := range *athletes {
		_, ok := athletesByYear[athlete.Year]
		if !ok {
			athletesByYear[athlete.Year] = make([]Athlete, 0)
		}
		athletesByYear[athlete.Year] = append(athletesByYear[athlete.Year], athlete)
	}
	for year, athletesByYear := range athletesByYear {
		athletesByCountry := make(map[string][]Athlete)
		for _, athlete := range athletesByYear {
			_, ok := athletesByCountry[athlete.Country]
			if !ok {
				athletesByCountry[athlete.Country] = make([]Athlete, 0)
			}
			athletesByCountry[athlete.Country] = append(athletesByCountry[athlete.Country], athlete)
		}
		MedalsByYear[year] = make(ByMedal, 0)
		for country, ath := range athletesByCountry {
			MedalsByYear[year] = append(MedalsByYear[year], createMedalsForCountry(ath, country))
		}
		sort.Sort(MedalsByYear[year])
	}
	countries := MedalsByYear[2008]
	print(countries)
}

func createAthleteData(athletes []Athlete) AthleteData {
	totalMedal := Medal{}
	allMedals := make(map[string]Medal)
	for _, athlete := range athletes {
		medal := Medal{Gold: athlete.Gold, Silver: athlete.Silver, Bronze: athlete.Bronze, Total: athlete.Total}
		allMedals[strconv.Itoa(athlete.Year)] = medal
		totalMedal.Total += medal.Total
		totalMedal.Gold += medal.Gold
		totalMedal.Silver += medal.Silver
		totalMedal.Bronze += medal.Bronze
	}
	return AthleteData{
		Athlete:      athletes[0].Athlete,
		Country:      athletes[0].Country,
		Medals:       totalMedal,
		MedalsByYear: allMedals,
	}
}

func createMedalsForCountry(athletes []Athlete, country string) MedalByCountry {
	result := MedalByCountry{Country: country}
	for _, athlete := range athletes {
		result.Total += athlete.Total
		result.Gold += athlete.Gold
		result.Silver += athlete.Silver
		result.Bronze += athlete.Bronze
	}
	return result
}

func (a ByMedal) Len() int {
	return len(a)
}

func (a ByMedal) Less(i, j int) bool {
	if a[i].Gold != a[j].Gold {
		return a[i].Gold > a[j].Gold
	}
	if a[i].Silver != a[j].Silver {
		return a[i].Silver > a[j].Silver
	}
	if a[i].Bronze != a[j].Bronze {
		return a[i].Bronze > a[j].Bronze
	}
	return a[i].Country < a[j].Country
}

func (a ByMedal) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func (a Athletes) Len() int {
	return len(a)
}

func (a Athletes) Less(i, j int) bool {
	if a[i].Medals.Gold != a[j].Medals.Gold {
		return a[i].Medals.Gold > a[j].Medals.Gold
	}
	if a[i].Medals.Silver != a[j].Medals.Silver {
		return a[i].Medals.Silver > a[j].Medals.Silver
	}
	if a[i].Medals.Bronze != a[j].Medals.Bronze {
		return a[i].Medals.Bronze > a[j].Medals.Bronze
	}
	return strings.Compare(a[i].Athlete, a[j].Athlete) < 1
}

func (a Athletes) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

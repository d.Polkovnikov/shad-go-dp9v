package main

import (
	"net/http"
	"regexp"
	"strings"
)

type RegexpHandler struct {
	routes []*route
}

func (h *RegexpHandler) HandleFunc(pattern string, method string, handler func(http.ResponseWriter, *http.Request)) {
	regex, _ := regexp.Compile(pattern)
	h.routes = append(h.routes, &route{regex, http.HandlerFunc(handler), strings.ToUpper(method)})
}

func (h *RegexpHandler) Handler(pattern *regexp.Regexp, method string, handler http.Handler) {
	h.routes = append(h.routes, &route{pattern, handler, method})
}

func (h *RegexpHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	for _, route := range h.routes {
		if route.pattern.MatchString(r.URL.Path) && r.Method == route.method {
			route.handler.ServeHTTP(w, r)
			return
		}
	}
	http.NotFound(w, r)
}

type route struct {
	pattern *regexp.Regexp
	handler http.Handler
	method  string
}

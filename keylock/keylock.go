// +build !solution

package keylock

import (
	"sync"
)

type KeyLock struct {
	lockedItems map[string]struct{}
	sync.Mutex
}

func New() *KeyLock {
	result := KeyLock{
		lockedItems: make(map[string]struct{}),
		Mutex:       sync.Mutex{},
	}
	return &result
}

func (l *KeyLock) LockKeys(keys []string, cancel <-chan struct{}) (canceled bool, unlock func()) {
	for {
		select {
		case <-cancel:
			return true, func() {}
		default:
			if l.checkAndLock(keys) {
				return false, func() { l.unlockItems(keys) }
			}
		}
	}
}

func (l *KeyLock) unlockItems(keys []string) {
	for _, key := range keys {
		l.Lock()
		delete(l.lockedItems, key)
		l.Unlock()
	}
}

func (l *KeyLock) checkAndLock(keys []string) bool {
	l.Lock()
	for _, key := range keys {
		_, ok := l.lockedItems[key]
		if ok {
			l.Unlock()
			return false
		}
	}
	for _, key := range keys {
		l.lockedItems[key] = struct{}{}
	}
	l.Unlock()
	return true
}

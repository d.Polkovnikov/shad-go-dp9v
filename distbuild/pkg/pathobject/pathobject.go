package pathobject

import (
	"gitlab.com/slon/shad-go/distbuild/pkg/build"
	"os"
	"path"
	"sync"
)

type PathObject struct {
	deleteLock  sync.Mutex
	isWriteLock sync.Mutex

	readersCount int32
	isRemoved    bool
	isDir        bool

	IsWrite     bool
	ID          build.ID
	FullDirName string
}

func (d *PathObject) CheckOrDelete() bool {
	d.deleteLock.Lock()
	defer d.deleteLock.Unlock()
	if d.readersCount > 0 {
		return false
	}
	d.isRemoved = true
	return true
}

func (d *PathObject) CheckOrAddReader() bool {
	d.deleteLock.Lock()
	defer d.deleteLock.Unlock()
	if d.isRemoved {
		return false
	}
	d.readersCount++
	return true
}

func (d *PathObject) RemoveReader() {
	d.deleteLock.Lock()
	defer d.deleteLock.Unlock()
	if d.readersCount == 0 {
		panic("0 active readers count")
	}
	d.readersCount--
}

func (d *PathObject) RemoveDirectory() bool {
	isDeleted := d.CheckOrDelete()
	if !isDeleted {
		return false
	}
	err := os.RemoveAll(d.FullDirName)
	if err != nil {
		d.isRemoved = false
		return false
	}
	return true
}

func (d *PathObject) FinishWrite() bool {
	d.isWriteLock.Lock()
	defer d.isWriteLock.Unlock()
	if !d.IsWrite {
		return false
	}
	d.IsWrite = false
	return true
}

func (d *PathObject) AbortWrite() bool {
	d.isWriteLock.Lock()
	defer d.isWriteLock.Unlock()
	if !d.IsWrite {
		return false
	}
	err := os.RemoveAll(d.FullDirName)
	if err != nil {
		return false
	}
	d.IsWrite = false
	return true
}

func CreateDirectory(id build.ID, rootPath string) (*PathObject, error) {
	dirName := id.String()
	fullDirName := path.Join(rootPath, dirName)
	err := os.Mkdir(fullDirName, 0777)
	if err != nil {
		return nil, err
	}
	return &PathObject{
		deleteLock:   sync.Mutex{},
		isWriteLock:  sync.Mutex{},
		ID:           id,
		readersCount: 0,
		IsWrite:      true,
		isRemoved:    false,
		FullDirName:  fullDirName,
	}, nil
}

func CreateFile(id build.ID, rootPath string) (*PathObject, error) {
	dirName := id.String()
	fullDirName := path.Join(rootPath, dirName)
	_, err := os.Create(fullDirName)
	if err != nil {
		return nil, err
	}
	return &PathObject{
		deleteLock:   sync.Mutex{},
		isWriteLock:  sync.Mutex{},
		ID:           id,
		readersCount: 0,
		IsWrite:      true,
		isRemoved:    false,
		FullDirName:  fullDirName,
	}, nil
}

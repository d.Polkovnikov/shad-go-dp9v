// +build !solution

package artifact

import (
	"errors"
	"gitlab.com/slon/shad-go/distbuild/pkg/build"
	"gitlab.com/slon/shad-go/distbuild/pkg/pathobject"
	"os"
	"sync"
)

var (
	ErrIoError     = errors.New("error")
	ErrNotFound    = errors.New("artifact not found")
	ErrExists      = errors.New("artifact exists")
	ErrWriteLocked = errors.New("artifact is locked for write")
	ErrReadLocked  = errors.New("artifact is locked for read")
)

type Cache struct {
	sync.Mutex
	rootDir   string
	artifacts map[build.ID]*artifact
}

type artifact struct {
	*pathobject.PathObject
	rootCache *Cache
}

func NewCache(root string) (*Cache, error) {
	dir, err := os.Open(root)
	if err != nil {
		return nil, err
	}
	defer dir.Close()
	return &Cache{
		Mutex:     sync.Mutex{},
		rootDir:   root,
		artifacts: make(map[build.ID]*artifact),
	}, nil
}

func (c *Cache) Range(artifactFn func(artifact build.ID) error) error {
	for id := range c.artifacts {
		err := artifactFn(id)
		if err != nil {
			return err
		}
	}
	return nil
}

func (c *Cache) Remove(artifactID build.ID) error {
	c.Lock()
	defer c.Unlock()
	a, ok := c.artifacts[artifactID]
	if !ok {
		return ErrNotFound
	}
	isDeleted := a.CheckOrDelete()
	if !isDeleted {
		return ErrReadLocked
	}
	delete(c.artifacts, artifactID)
	err := os.RemoveAll(a.FullDirName)
	if err != nil {
		panic(err.Error())
	}
	return nil
}

func (c *Cache) Create(artifactID build.ID) (path string, commit, abort func() error, err error) {
	c.Lock()
	defer c.Unlock()
	a, ok := c.artifacts[artifactID]
	if ok {
		if a.IsWrite {
			return "", emptyErr, emptyErr, ErrWriteLocked
		}
		return "", emptyErr, emptyErr, ErrExists
	}
	directory, err := pathobject.CreateDirectory(artifactID, c.rootDir)
	if err != nil {
		return "", emptyErr, emptyErr, err
	}
	newArtifact := &artifact{
		PathObject: directory,
		rootCache:  c,
	}
	c.artifacts[artifactID] = newArtifact
	return newArtifact.FullDirName, newArtifact.commit, newArtifact.abort, nil
}

func (c *Cache) Get(artifactID build.ID) (path string, unlock func(), err error) {
	c.Lock()
	defer c.Unlock()
	a, ok := c.artifacts[artifactID]
	if !ok {
		return "", func() {}, ErrNotFound
	}
	success := a.CheckOrAddReader()
	if !success {
		return "", func() {}, ErrWriteLocked
	}
	return a.FullDirName, a.unlock, nil
}

func (a *artifact) commit() error {
	writeSuccess := a.FinishWrite()
	if !writeSuccess {
		return ErrIoError
	}
	return nil
}

func (a *artifact) abort() error {
	writeSuccess := a.AbortWrite()
	if !writeSuccess {
		return ErrIoError
	}
	delete(a.rootCache.artifacts, a.ID)
	return nil
}

func (a *artifact) unlock() {
	a.RemoveReader()
}

func emptyErr() error {
	return errors.New("NOT SUPPORTED")
}

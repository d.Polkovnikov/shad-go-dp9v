// +build !solution

package artifact

import (
	"errors"
	"gitlab.com/slon/shad-go/distbuild/pkg/build"
	"gitlab.com/slon/shad-go/distbuild/pkg/tarstream"
	"net/http"

	"go.uber.org/zap"
)

type Handler struct {
	cache  *Cache
	logger *zap.Logger
}

func NewHandler(l *zap.Logger, c *Cache) *Handler {
	return &Handler{
		cache:  c,
		logger: l,
	}
}

func (h *Handler) Register(mux *http.ServeMux) {
	mux.HandleFunc("/artifact", h.handle)
}

func (h *Handler) handle(writer http.ResponseWriter, request *http.Request) {
	rawID := request.URL.Query().Get("id")
	if len(rawID) < 1 {
		http.Error(writer, "id is empty", http.StatusNotFound)
		return
	}
	id := build.ID{}
	err := id.UnmarshalText([]byte(rawID))
	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}
	get, unlock, err := h.cache.Get(id)
	if err != nil && errors.Is(err, ErrNotFound) {
		http.Error(writer, "artifact not found", http.StatusNotFound)
		return
	}
	defer unlock()
	err = tarstream.Send(get, writer)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}
}

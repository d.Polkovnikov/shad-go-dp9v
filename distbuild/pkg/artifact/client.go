// +build !solution

package artifact

import (
	"context"
	"fmt"
	"gitlab.com/slon/shad-go/distbuild/pkg/tarstream"
	"net/http"

	"gitlab.com/slon/shad-go/distbuild/pkg/build"
)

const path = "artifact"

// Download artifact from remote cache into local cache.
func Download(ctx context.Context, endpoint string, c *Cache, artifactID build.ID) error {
	requestURL := fmt.Sprintf("%s/%s?id=%s", endpoint, path, artifactID.String())
	client := http.Client{}
	request, err := http.NewRequestWithContext(ctx, http.MethodGet, requestURL, nil)
	if err != nil {
		return err
	}
	response, _ := client.Do(request)
	path, commit, abort, err := c.Create(artifactID)
	if err != nil {
		return err
	}
	err = tarstream.Receive(path, response.Body)
	if err != nil {
		_ = abort()
		return err
	}
	err = commit()
	return err
}

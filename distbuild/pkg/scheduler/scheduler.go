// +build !solution

package scheduler

import (
	"context"
	"math/rand"
	"sync"
	"time"

	"go.uber.org/zap"

	"gitlab.com/slon/shad-go/distbuild/pkg/api"
	"gitlab.com/slon/shad-go/distbuild/pkg/build"
)

var timeAfter = time.After

type PendingJob struct {
	Job      *api.JobSpec
	Finished chan struct{}
	Result   *api.JobResult
}

type Config struct {
	CacheTimeout time.Duration
	DepsTimeout  time.Duration
}

type Scheduler struct {
	sync.Mutex
	Config
	workerWithCache map[build.ID][]api.WorkerID
	globalQueue     chan build.ID
	workersQueues   map[api.WorkerID]*WorkerQueues
	pendingJobs     map[build.ID]*PendingJob
}

type WorkerQueues struct {
	First  chan build.ID
	Second chan build.ID
}

type scheduledJob struct {
	api.JobSpec
}

func NewScheduler(l *zap.Logger, config Config) *Scheduler {
	return &Scheduler{
		Mutex:           sync.Mutex{},
		Config:          config,
		workerWithCache: make(map[build.ID][]api.WorkerID),
		globalQueue:     make(chan build.ID),
		workersQueues:   make(map[api.WorkerID]*WorkerQueues),
		pendingJobs:     make(map[build.ID]*PendingJob),
	}
}

func (c *Scheduler) LocateArtifact(id build.ID) (api.WorkerID, bool) {
	c.Lock()
	defer c.Unlock()
	workers, ok := c.workerWithCache[id]
	if !ok {
		return "", false
	}
	rndNum := rand.Intn(len(workers))
	return workers[rndNum], true
}

func (c *Scheduler) RegisterWorker(workerID api.WorkerID) {
	queues := WorkerQueues{
		First:  make(chan build.ID),
		Second: make(chan build.ID),
	}
	c.Lock()
	defer c.Unlock()
	c.workersQueues[workerID] = &queues
}

func (c *Scheduler) OnJobComplete(workerID api.WorkerID, jobID build.ID, res *api.JobResult) bool {
	c.Lock()
	defer c.Unlock()
	ids, ok := c.workerWithCache[jobID]
	if !ok {
		ids = make([]api.WorkerID, 0)
	}
	ids = append(ids, workerID)
	c.workerWithCache[jobID] = ids

	job, ok := c.pendingJobs[jobID]
	if !ok {
		job = &PendingJob{
			Finished: make(chan struct{}),
		}
	}
	job.Result = res
	close(job.Finished)
	c.pendingJobs[jobID] = job
	return true
}

func (c *Scheduler) ScheduleJob(job *api.JobSpec) *PendingJob {
	pendingJob := &PendingJob{
		Job:      job,
		Finished: make(chan struct{}),
		Result:   nil,
	}
	c.Lock()
	defer c.Unlock()
	c.pendingJobs[job.ID] = pendingJob
	go c.scheduleJob(job.ID)
	return pendingJob
}

func (c *Scheduler) scheduleJob(id build.ID) {
	select {
	case <-timeAfter(c.Config.CacheTimeout):
	case c.globalQueue <- id:
		return
	}
	select {
	case <-timeAfter(c.Config.DepsTimeout):
	case c.globalQueue <- id:
		return
	}
	c.globalQueue <- id
}

func (c *Scheduler) PickJob(ctx context.Context, workerID api.WorkerID) *PendingJob {
	select {
	case <-ctx.Done():
		return nil
	case jobID := <-c.globalQueue:
		return c.pendingJobs[jobID]
	}
}

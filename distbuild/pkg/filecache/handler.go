// +build !solution

package filecache

import (
	"errors"
	"gitlab.com/slon/shad-go/distbuild/pkg/build"
	"golang.org/x/sync/singleflight"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
)

type Handler struct {
	cache  *Cache
	logger *zap.Logger
	group  singleflight.Group
}

func NewHandler(l *zap.Logger, c *Cache) *Handler {
	return &Handler{
		cache:  c,
		logger: l,
		group:  singleflight.Group{},
	}
}

func (h *Handler) Register(mux *http.ServeMux) {
	mux.HandleFunc("/file", h.handle)
}

func (h *Handler) handle(writer http.ResponseWriter, request *http.Request) {
	rawID := request.URL.Query().Get("id")
	if len(rawID) < 1 {
		http.Error(writer, "id is empty", http.StatusNotFound)
		return
	}
	id := build.ID{}
	err := id.UnmarshalText([]byte(rawID))
	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}

	switch request.Method {
	case http.MethodGet:
		h.handleGet(writer, id)
	case http.MethodPut:
		h.handlePut(writer, request, id)
	default:
		http.NotFound(writer, request)
	}
}

func (h *Handler) handleGet(writer http.ResponseWriter, id build.ID) {
	file, unlock, err := h.cache.Get(id)
	if err != nil && errors.Is(err, ErrNotFound) {
		http.Error(writer, "artifact not found", http.StatusNotFound)
		return
	}
	defer unlock()
	body, err := ioutil.ReadFile(file)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
	}
	_, err = writer.Write(body)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
	}
}

func (h *Handler) handlePut(writer http.ResponseWriter, request *http.Request, file build.ID) { //todo: обработать параллельную загрузку
	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err, _ = h.group.Do(file.String(), func() (i interface{}, err error) {
		err = h.writeFile(body, file)
		return nil, err
	})

	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
	}
}

func (h *Handler) writeFile(body []byte, id build.ID) error {
	_, unlock, err := h.cache.Get(id)
	if err == nil {
		unlock()
		return err
	}

	write, abort, err := h.cache.Write(id)
	defer func() { _ = write.Close() }()
	if err != nil {
		return err
	}

	_, err = write.Write(body)
	if err != nil {
		_ = abort()
		return err
	}
	return nil
}

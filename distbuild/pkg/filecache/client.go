// +build !solution

package filecache

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"go.uber.org/zap"

	"gitlab.com/slon/shad-go/distbuild/pkg/build"
)

const pathFormat = "%s/file?id="

type Client struct {
	pathFormat string
	logger     *zap.Logger
	http.Client
}

func NewClient(l *zap.Logger, endpoint string) *Client {
	return &Client{
		pathFormat: fmt.Sprintf(pathFormat, endpoint) + "%s",
		logger:     l,
		Client:     http.Client{},
	}
}

func (c *Client) Upload(ctx context.Context, id build.ID, localPath string) error {
	file, err := os.Open(localPath)
	if err != nil {
		return err
	}
	path := fmt.Sprintf(c.pathFormat, id.String())
	request, _ := http.NewRequestWithContext(ctx, http.MethodPut, path, file)
	_, err = c.Do(request)
	if err != nil {
		return err
	}
	return nil
}

func (c *Client) Download(ctx context.Context, localCache *Cache, id build.ID) error {
	requestURL := fmt.Sprintf(c.pathFormat, id.String())
	request, err := http.NewRequestWithContext(ctx, http.MethodGet, requestURL, nil)
	if err != nil {
		return err
	}
	response, err := c.Do(request)
	if err != nil {
		return err
	}
	if response.StatusCode == http.StatusInternalServerError || response.StatusCode == http.StatusNotFound {
		return errors.New("NOT FOUND")
	}
	write, abort, err := localCache.Write(id)
	if err != nil {
		return err
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		_ = abort()
		return err
	}
	_, err = write.Write(body)
	if err != nil {
		_ = abort()
		return err
	}
	err = write.Close()
	if err != nil {
		return err
	}
	return nil
}

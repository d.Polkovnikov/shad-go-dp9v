// +build !solution

package filecache

import (
	"errors"
	"gitlab.com/slon/shad-go/distbuild/pkg/pathobject"
	"io"
	"io/ioutil"
	"os"
	"sync"

	"gitlab.com/slon/shad-go/distbuild/pkg/build"
)

var (
	ErrIoError     = errors.New("error")
	ErrNotFound    = errors.New("File not found")
	ErrExists      = errors.New("File exists")
	ErrWriteLocked = errors.New("File is locked for write")
	ErrReadLocked  = errors.New("File is locked for read")
)

type Cache struct {
	sync.Mutex
	rootDir   string
	artifacts map[build.ID]*File
}

type File struct {
	*pathobject.PathObject
	rootCache *Cache
}

func New(rootDir string) (*Cache, error) {
	dir, err := os.Open(rootDir)
	if err != nil {
		return nil, err
	}
	defer dir.Close()
	return &Cache{
		Mutex:     sync.Mutex{},
		rootDir:   rootDir,
		artifacts: make(map[build.ID]*File),
	}, nil
}

func (c *Cache) Range(fileFn func(file build.ID) error) error {
	for id := range c.artifacts {
		err := fileFn(id)
		if err != nil {
			return err
		}
	}
	return nil
}

func (c *Cache) Remove(fileID build.ID) error {
	c.Lock()
	defer c.Unlock()
	a, ok := c.artifacts[fileID]
	if !ok {
		return ErrNotFound
	}
	isDeleted := a.CheckOrDelete()
	if !isDeleted {
		return ErrReadLocked
	}
	delete(c.artifacts, fileID)
	err := os.RemoveAll(a.FullDirName)
	if err != nil {
		panic(err.Error())
	}
	return nil
}

func (c *Cache) Write(fileID build.ID) (w io.WriteCloser, abort func() error, err error) {
	c.Lock()
	defer c.Unlock()
	a, ok := c.artifacts[fileID]
	if ok {
		if a.IsWrite {
			return nil, emptyErr, ErrWriteLocked
		}
		return nil, emptyErr, ErrExists
	}
	directory, err := pathobject.CreateFile(fileID, c.rootDir)
	if err != nil {
		return nil, emptyErr, err
	}
	newFile := &File{
		PathObject: directory,
		rootCache:  c,
	}
	c.artifacts[fileID] = newFile
	return newFile, newFile.abort, nil
}

func (c *Cache) Get(fileID build.ID) (path string, unlock func(), err error) {
	c.Lock()
	defer c.Unlock()
	a, ok := c.artifacts[fileID]
	if !ok {
		return "", func() {}, ErrNotFound
	}
	success := a.CheckOrAddReader()
	if !success {
		return "", func() {}, ErrWriteLocked
	}
	return a.FullDirName, a.unlock, nil
}

func (f *File) abort() error {
	writeSuccess := f.AbortWrite()
	if !writeSuccess {
		return ErrIoError
	}
	delete(f.rootCache.artifacts, f.ID)
	return nil
}

func (f *File) unlock() {
	f.RemoveReader()
}

func emptyErr() error {
	return errors.New("NOT SUPPORTED")
}

func (f *File) Write(p []byte) (n int, err error) {
	err = ioutil.WriteFile(f.FullDirName, p, 0777)
	if err != nil {
		return 0, err
	}
	return len(p), nil
}

func (f *File) Close() error {
	writeSuccess := f.FinishWrite()
	if !writeSuccess {
		return ErrIoError
	}
	return nil
}

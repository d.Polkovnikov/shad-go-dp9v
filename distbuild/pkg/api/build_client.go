// +build !solution

package api

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"

	"gitlab.com/slon/shad-go/distbuild/pkg/build"
)

const (
	SignalBuildURLFormat = "%s/signal?build_id=%s"
	StartBuildURLFormat  = "%s/build"
)

type BuildClient struct {
	l        *zap.Logger
	endpoint string
	http.Client
}

func NewBuildClient(l *zap.Logger, endpoint string) *BuildClient {
	return &BuildClient{
		l:        l,
		endpoint: endpoint,
		Client:   http.Client{},
	}
}

func (c *BuildClient) StartBuild(ctx context.Context, buildRequest *BuildRequest) (*BuildStarted, StatusReader, error) {
	url := fmt.Sprintf(StartBuildURLFormat, c.endpoint)

	buffer := bytes.NewBuffer(make([]byte, 0))
	encoder := json.NewEncoder(buffer)
	err := encoder.Encode(buildRequest)
	if err != nil {
		return nil, nil, err
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, url, buffer)

	if err != nil {
		return nil, nil, err
	}
	resp, err := c.Do(request)
	if err != nil {
		return nil, nil, err
	}
	if resp.StatusCode == http.StatusInternalServerError {
		errorMsg, _ := ioutil.ReadAll(resp.Body)
		return nil, nil, errors.New(string(errorMsg))
	}
	reader := NewMyStatusReader(resp.Body)
	start, err := reader.start()
	if err != nil {
		return nil, nil, err
	}
	return start, reader, nil
}

func (c *BuildClient) SignalBuild(ctx context.Context, buildID build.ID, signal *SignalRequest) (*SignalResponse, error) {
	url := fmt.Sprintf(SignalBuildURLFormat, c.endpoint, buildID.String())

	buffer := bytes.NewBuffer(make([]byte, 0))
	encoder := json.NewEncoder(buffer)
	err := encoder.Encode(signal)
	if err != nil {
		return nil, err
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, url, buffer)

	if err != nil {
		return nil, err
	}
	resp, err := c.Do(request)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode == http.StatusNotFound || resp.StatusCode == http.StatusInternalServerError {
		all, _ := ioutil.ReadAll(resp.Body)
		return nil, errors.New(string(all))
	}

	var signalResponse SignalResponse
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&signalResponse)
	if err != nil {
		return nil, err
	}
	return &signalResponse, nil
}

// +build !solution

package api

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
)

const pathFormat = "%s/heartbeat"

type HeartbeatClient struct {
	l    *zap.Logger
	path string
	http.Client
}

func NewHeartbeatClient(l *zap.Logger, endpoint string) *HeartbeatClient {
	return &HeartbeatClient{
		l:      l,
		path:   fmt.Sprintf(pathFormat, endpoint),
		Client: http.Client{},
	}
}

func (c *HeartbeatClient) Heartbeat(ctx context.Context, req *HeartbeatRequest) (*HeartbeatResponse, error) {
	buffer := bytes.NewBuffer(make([]byte, 0))
	encoder := json.NewEncoder(buffer)
	err := encoder.Encode(req)
	if err != nil {
		return nil, err
	}
	reqeust, err := http.NewRequestWithContext(ctx, http.MethodPost, c.path, buffer)
	if err != nil {
		return nil, err
	}
	resp, err := c.Do(reqeust)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode == http.StatusNotFound || resp.StatusCode == http.StatusInternalServerError {
		all, _ := ioutil.ReadAll(resp.Body)
		return nil, errors.New(string(all))
	}

	var heartbeatResp HeartbeatResponse
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&heartbeatResp)
	if err != nil {
		return nil, err
	}
	return &heartbeatResp, nil
}

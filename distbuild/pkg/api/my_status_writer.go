package api

import (
	"encoding/json"
	"net/http"
)

type MyStatusWriter struct {
	http.ResponseWriter
	IsStarted bool
	e         *json.Encoder
}

func createMyStatusWriter(w http.ResponseWriter) *MyStatusWriter {
	encoder := json.NewEncoder(w)
	return &MyStatusWriter{
		ResponseWriter: w,
		IsStarted:      false,
		e:              encoder,
	}
}

func (m *MyStatusWriter) Started(rsp *BuildStarted) error {
	defer m.flush()
	m.IsStarted = true
	return m.e.Encode(rsp)
}

func (m *MyStatusWriter) Updated(update *StatusUpdate) error {
	defer m.flush()
	err := m.e.Encode(update)
	if err != nil {
		return nil
	}
	return nil
}

func (m *MyStatusWriter) flush() {
	flusher := m.ResponseWriter.(http.Flusher)
	flusher.Flush()
}

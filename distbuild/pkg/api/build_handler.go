// +build !solution

package api

import (
	"encoding/json"
	"gitlab.com/slon/shad-go/distbuild/pkg/build"
	"net/http"

	"go.uber.org/zap"
)

func NewBuildService(l *zap.Logger, s Service) *BuildHandler {
	return &BuildHandler{
		s: s,
		l: l,
	}
}

type BuildHandler struct {
	s Service
	l *zap.Logger
}

func (h *BuildHandler) Register(mux *http.ServeMux) {
	mux.HandleFunc("/signal", h.signalBuildHandler)
	mux.HandleFunc("/build", h.startBuildHandler)
}

func (h *BuildHandler) startBuildHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.NotFound(w, r)
		return
	}

	var bRequest BuildRequest
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&bRequest)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	myWriter := createMyStatusWriter(w)

	err = h.s.StartBuild(r.Context(), &bRequest, myWriter)
	if err != nil {
		if myWriter.IsStarted {
			failedStatus := &StatusUpdate{
				BuildFailed: &BuildFailed{Error: err.Error()},
			}
			_ = myWriter.Updated(failedStatus)
		} else {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	}
}

func (h *BuildHandler) signalBuildHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.NotFound(w, r)
		return
	}

	rawBuildID := r.URL.Query().Get("build_id")
	buildID := build.ID{}
	err := buildID.UnmarshalText([]byte(rawBuildID))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var signal SignalRequest
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&signal)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	signalBuild, err := h.s.SignalBuild(r.Context(), buildID, &signal)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	encoder := json.NewEncoder(w)
	err = encoder.Encode(&signalBuild)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

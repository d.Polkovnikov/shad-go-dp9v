package api

import (
	"encoding/json"
	"errors"
	"io"
)

type MyStatusReader struct {
	io.ReadCloser
	isClosed  bool
	isStarted bool
	decoder   *json.Decoder
}

func NewMyStatusReader(rc io.ReadCloser) *MyStatusReader {
	return &MyStatusReader{
		ReadCloser: rc,
		isClosed:   false,
		decoder:    json.NewDecoder(rc),
	}
}

func (m MyStatusReader) Close() error {
	if m.isClosed {
		return errors.New("reader was closed")
	}
	m.isClosed = true
	return m.ReadCloser.Close()
}

func (m MyStatusReader) Next() (*StatusUpdate, error) {
	var statusUpdate StatusUpdate
	if !m.decoder.More() {
		return nil, io.EOF
	}
	err := m.decoder.Decode(&statusUpdate)
	if err != nil {
		return nil, err
	}
	return &statusUpdate, nil
}

func (m MyStatusReader) start() (*BuildStarted, error) {
	var buildStarted BuildStarted
	err := m.decoder.Decode(&buildStarted)
	if err != nil {
		return nil, err
	}
	return &buildStarted, nil
}

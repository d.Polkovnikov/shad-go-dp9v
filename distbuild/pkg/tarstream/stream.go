// +build !solution

package tarstream

import (
	"archive/tar"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

func Send(dir string, w io.Writer) error {
	tw := tar.NewWriter(w)
	defer func() { _ = tw.Close() }()

	err := filepath.Walk(dir, func(path string, info os.FileInfo, e error) error {
		infoHeader, err := tar.FileInfoHeader(info, info.Name())
		if err != nil {
			return err
		}
		infoHeader.Name = strings.TrimPrefix(strings.Replace(path, dir, "", -1), string(filepath.Separator))
		_ = tw.WriteHeader(infoHeader)
		if !info.IsDir() {
			file, _ := ioutil.ReadFile(path)
			_, _ = tw.Write(file)
		}
		_ = tw.Flush()
		return nil
	})
	return err
}

func Receive(dir string, r io.Reader) error {
	reader := tar.NewReader(r)
	for {
		next, err := reader.Next()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			return err
		}
		newFileName := filepath.Join(dir, next.Name)
		bytes := make([]byte, next.Size)
		_, _ = reader.Read(bytes)
		if newFileName == dir {
			continue
		}
		if next.FileInfo().IsDir() {
			err = os.Mkdir(newFileName, next.FileInfo().Mode())
		} else {
			err = ioutil.WriteFile(newFileName, bytes, next.FileInfo().Mode())
		}
		if err != nil {
			return err
		}
	}
}

package web

import (
	"errors"
	"log"
	"net/http"
)

func RunServer(address, serviceAddr, configPath string) error {
	if len(address) == 0 || len(serviceAddr) == 0 || len(configPath) == 0 {
		return errors.New("no data")
	}
	handler, err := CreateHandler(serviceAddr, configPath)
	if err != nil {
		return err
	}
	http.HandleFunc("/", handler.handle)
	log.Fatal(http.ListenAndServe(address, nil))
	return nil
}

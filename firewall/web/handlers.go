package web

import (
	"bytes"
	"errors"
	"gitlab.com/slon/shad-go/firewall/internal/checker"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"time"
)

type MyHandler struct {
	outPath string
	config  checker.Config
	http.RoundTripper
}

func CreateHandler(outPath, configPath string) (*MyHandler, error) {
	config, err := checker.LoadFromPath(configPath)
	if err != nil {
		return nil, errors.New("config not found")
	}
	transport := &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		DialContext: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
		}).DialContext,
		MaxIdleConns:          100,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
	}
	return &MyHandler{
		outPath:      outPath,
		config:       config,
		RoundTripper: transport,
	}, nil
}

func (m *MyHandler) handle(w http.ResponseWriter, r *http.Request) {
	body, ok := m.config.ReadAndCheckRequest(r)
	if !ok {
		http.Error(w, "Forbidden", 403)
		return
	}

	r, err := makeRequest(r, body, m.outPath)
	if err != nil {
		http.Error(w, "SERVER ERROR", http.StatusInternalServerError)
		return
	}

	response, err := m.RoundTrip(r)
	if err != nil {
		http.Error(w, "SERVER ERROR", http.StatusInternalServerError)
		return
	}

	body, ok = m.config.ReadAndCheckResponse(r, response)
	if !ok {
		http.Error(w, "Forbidden", 403)
		return
	}
	_, _ = w.Write(body)
	w.WriteHeader(response.StatusCode)
}

func makeRequest(r *http.Request, body []byte, path string) (*http.Request, error) {
	newURL, err := url.Parse(path)
	if err != nil {
		return nil, err
	}
	newURL.Path = r.URL.Path
	r.URL = newURL
	r.Host = path
	readerBody := bytes.NewReader(body)
	r.Body = ioutil.NopCloser(readerBody)
	return r, nil
}

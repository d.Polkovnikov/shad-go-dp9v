// +build !solution

package main

import (
	"flag"
	"gitlab.com/slon/shad-go/firewall/web"
)

var (
	serviceAddr = flag.String("service-addr", "", "")
	conf        = flag.String("conf", "", "")
	addr        = flag.String("addr", "", "")
)

func main() {
	flag.Parse()
	err := web.RunServer(*addr, *serviceAddr, *conf)
	if err != nil {
		panic(err.Error())
	}
}

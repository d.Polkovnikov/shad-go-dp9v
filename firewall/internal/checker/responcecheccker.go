package checker

import (
	"io/ioutil"
	"net/http"
	"regexp"
)

func (c Config) ReadAndCheckResponse(request *http.Request, response *http.Response) ([]byte, bool) {
	body, _ := ioutil.ReadAll(response.Body)
	rule, ok := c[request.URL.Path]
	if !ok {
		return body, true
	}
	return body, rule.checkForbiddenResponseCodes(response) &&
		rule.checkResponseLength(response) &&
		rule.checkForbiddenResponse(body)
}

func (r *Rule) checkForbiddenResponseCodes(response *http.Response) bool {
	code := response.StatusCode
	for _, responseCode := range r.ForbiddenResponseCodes {
		if responseCode == code {
			return false
		}
	}
	return true
}

func (r *Rule) checkResponseLength(response *http.Response) bool {
	respLength := response.ContentLength
	return r.MaxResponseLengthBytes == 0 || respLength < r.MaxResponseLengthBytes
}

func (r *Rule) checkForbiddenResponse(body []byte) bool {
	for _, re := range r.ForbiddenResponseRe {
		matchString, err := regexp.Match(re, body)
		if err != nil || matchString {
			return false
		}
	}
	return true
}

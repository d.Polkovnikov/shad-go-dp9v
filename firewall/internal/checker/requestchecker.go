package checker

import (
	"io/ioutil"
	"net/http"
	"regexp"
	"strings"
)

func (c Config) ReadAndCheckRequest(request *http.Request) ([]byte, bool) {
	body, _ := ioutil.ReadAll(request.Body)
	_ = request.Body.Close()
	rule, ok := c[request.URL.Path]
	if !ok {
		return body, true
	}
	return body, rule.checkForbiddenUserAgents(request) &&
		rule.checkForbiddenHeaders(request) &&
		rule.checkRequiredHeaders(request) &&
		rule.checkRequestLength(request) &&
		rule.checkForbiddenRequest(body)
}

func (r *Rule) checkForbiddenUserAgents(request *http.Request) bool {
	userAgent := request.Header.Get("User-Agent")
	for _, pattern := range r.ForbiddenUserAgents {
		matchString, err := regexp.MatchString(pattern, userAgent)
		if err != nil || matchString {
			return false
		}
	}
	return true
}

func (r *Rule) checkForbiddenHeaders(request *http.Request) bool {
	for _, header := range r.ForbiddenHeaders {
		items := strings.Split(header, ": ")
		headerValue := request.Header.Get(items[0])
		matchString, err := regexp.MatchString(items[1], headerValue)
		if err != nil || matchString {
			return false
		}
	}
	return true
}

func (r *Rule) checkRequiredHeaders(request *http.Request) bool {
	for _, header := range r.RequiredHeaders {
		headerValue := request.Header.Get(header)
		if len(headerValue) == 0 {
			return false
		}
	}
	return true
}

func (r *Rule) checkRequestLength(request *http.Request) bool {
	return r.MaxRequestLengthBytes == 0 || request.ContentLength <= r.MaxRequestLengthBytes
}

func (r *Rule) checkForbiddenRequest(body []byte) bool {
	for _, re := range r.ForbiddenRequestRe {
		matchString, err := regexp.Match(re, body)
		if err != nil || matchString {
			return false
		}
	}
	return true
}

package checker

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type Config map[string]Rule

type Rule struct {
	Endpoint              string   `yaml:"endpoint"`
	ForbiddenUserAgents   []string `yaml:"forbidden_user_agents"`
	ForbiddenHeaders      []string `yaml:"forbidden_headers"`
	RequiredHeaders       []string `yaml:"required_headers"`
	MaxRequestLengthBytes int64    `yaml:"max_request_length_bytes"`
	ForbiddenRequestRe    []string `yaml:"forbidden_request_re"`

	MaxResponseLengthBytes int64    `yaml:"max_response_length_bytes"`
	ForbiddenResponseCodes []int    `yaml:"forbidden_response_codes"`
	ForbiddenResponseRe    []string `yaml:"forbidden_response_re"`
}

type importConfig struct {
	Rules []Rule `yaml:"rules"`
}

func LoadFromPath(path string) (Config, error) {
	content, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	config := importConfig{}
	err = yaml.Unmarshal(content, &config)
	if err != nil {
		return nil, err
	}
	res := make(map[string]Rule)
	for _, rule := range config.Rules {
		res[rule.Endpoint] = rule
	}
	return res, nil
}

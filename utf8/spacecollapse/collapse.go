// +build !solution

package spacecollapse

import "unicode"

func CollapseSpaces(input string) string {
	result := make([]rune, 0)
	prevIsSpace := false
	for _, val := range input {
		if unicode.IsSpace(val) {
			if !prevIsSpace {
				prevIsSpace = true
				result = append(result, rune(' '))
			}
		} else {
			prevIsSpace = false
			result = append(result, val)
		}
	}
	return string(result)
}

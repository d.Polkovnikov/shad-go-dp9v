// +build !solution

package reverse

func Reverse(input string) string {
	x := []rune(input)
	size := len(x)
	result := make([]rune, size)
	for i, val := range x {
		result[size-1-i] = val
	}
	return string(result)
}

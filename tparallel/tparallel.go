// +build !solution

package tparallel

type T struct {
	parent            *T
	subTestsLock      chan bool
	parentLockChannel chan bool
	subTests          []*T
	isParallel        bool
}

func (t *T) Parallel() {
	t.isParallel = true
	t.parent.subTests = append(t.parent.subTests, t)
	t.parentLockChannel <- true
	<-t.parent.subTestsLock
}

func (t *T) Run(subtest func(t *T)) {
	t = &T{
		subTestsLock:      make(chan bool),
		parentLockChannel: make(chan bool),
		parent:            t,
	}
	go tRun(t, subtest)
	<-t.parentLockChannel
}

func Run(topTests []func(t *T)) {
	t := &T{
		subTestsLock:      make(chan bool),
		parentLockChannel: make(chan bool),
		parent:            nil,
	}
	for _, test := range topTests {
		t.Run(test)
	}
	waitSubTests(t)
}

func tRun(t *T, fn func(t *T)) {
	fn(t)
	waitSubTests(t)
	t.parentLockChannel <- true
}

func waitSubTests(t *T) {
	if len(t.subTests) > 0 {
		close(t.subTestsLock)
		for _, sub := range t.subTests {
			<-sub.parentLockChannel
		}
	}
}

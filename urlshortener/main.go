// +build !solution

package main

import (
	"flag"
	"log"
	"net/http"
)

const (
	PostURLRegex = "/shorten"
	URLLength    = 10
	GetURLRegex  = "^/go/(.*)$"
)

var (
	port = flag.String("port", "8000", "")
)

func main() {
	flag.Parse()
	x := RegexpHandler{}
	x.HandleFunc(PostURLRegex, "POST", handlePost)
	x.HandleFunc(GetURLRegex, "GET", handleGet)
	log.Fatal(http.ListenAndServe("localhost:"+*port, &x))
}

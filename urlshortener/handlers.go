package main

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"regexp"
	"sync"
)

var (
	shortToURL = make(map[string]string)
	urlToShort = make(map[string]string)
	mu         sync.Mutex
)

type Request struct {
	URL string
}

type Response struct {
	URL string
	Key string
}

func handlePost(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var req Request
	err := decoder.Decode(&req)
	if err != nil {
		http.Error(w, "Error", http.StatusBadRequest)
	}
	url := addURL(req)
	response, err := json.Marshal(url)
	if err != nil {
		http.Error(w, "Error", http.StatusBadRequest)
	}
	w.Header().Add("Content-Type", "application/json")
	_, _ = fmt.Fprintf(w, "%s", response)
}

func handleGet(w http.ResponseWriter, r *http.Request) {
	key := getShortURLKey(r)
	url, ok := shortToURL[key]
	if !ok {
		http.NotFound(w, r)
	}
	http.Redirect(w, r, url, http.StatusFound)
}

func addURL(urlRequest Request) Response {
	url := urlRequest.URL
	shortURL, ok := urlToShort[url]
	if ok {
		return Response{URL: url, Key: shortURL}
	}
	shortURL = generateString()
	_, ok = shortToURL[shortURL]
	for ok {
		shortURL = generateString()
		_, ok = shortToURL[shortURL]
	}
	mu.Lock()
	shortToURL[shortURL] = url
	urlToShort[url] = shortURL
	mu.Unlock()
	return Response{url, shortURL}
}

func generateString() string {
	charset := "abcdefghijklmnopqrstuvwxyz1234567890"
	b := make([]byte, URLLength)
	for i := range b {
		b[i] = charset[rand.Intn(len(charset))]
	}
	return string(b)
}

func getShortURLKey(r *http.Request) string {
	re, _ := regexp.Compile(GetURLRegex)
	return re.FindStringSubmatch(r.URL.Path)[1]
}

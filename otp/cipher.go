// +build !solution

package otp

import (
	"io"
)

func NewReader(r io.Reader, prng io.Reader) io.Reader {
	return MyReader{
		r:    r,
		prng: prng,
	}
}

func NewWriter(w io.Writer, prng io.Reader) io.Writer {
	return MyWriter{
		w:    w,
		prng: prng,
	}
}

type MyWriter struct {
	w    io.Writer
	prng io.Reader
}

type MyReader struct {
	r    io.Reader
	prng io.Reader
}

func (w MyWriter) Write(p []byte) (n int, err error) {
	length := len(p)
	prngValues := readPrng(w.prng, length)

	result := make([]byte, length)
	for i, val := range prngValues {
		result[i] = p[i] ^ val
	}
	return w.w.Write(result)
}

func (r MyReader) Read(p []byte) (n int, err error) {
	n, err = r.r.Read(p)
	if n == 0 && err != nil && err != io.EOF {
		return 0, err
	}
	prngValues := readPrng(r.prng, n)
	for i := 0; i < n; i++ {
		p[i] = p[i] ^ prngValues[i]
	}

	if err == io.EOF {
		return n, io.EOF
	}
	return n, nil
}

func readPrng(prng io.Reader, length int) []byte {
	prngValues := make([]byte, length)
	_, _ = prng.Read(prngValues)
	return prngValues
}

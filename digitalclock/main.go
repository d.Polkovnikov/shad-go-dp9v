// +build !solution

package main

import (
	"errors"
	"flag"
	"image"
	"image/color"
	"image/png"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

const (
	patternHeight = 12
	timePattern   = "15:04:05"
)

var (
	White = color.RGBA{R: 255, G: 255, B: 255, A: 0xff}
	w, h  int
	port  = flag.String("port", "8000", "")
)

var symbolPatterns = map[int32]string{
	'1': One,
	'2': Two,
	'3': Three,
	'4': Four,
	'5': Five,
	'6': Six,
	'7': Seven,
	'8': Eight,
	'9': Nine,
	'0': Zero,
	':': Colon,
}

func main() {
	flag.Parse()
	http.HandleFunc("/", timeHandler) // each request calls handler
	log.Fatal(http.ListenAndServe("localhost:"+*port, nil))
}

func timeHandler(w http.ResponseWriter, r *http.Request) {
	k, timeString, err := getArguments(*r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	img := createImage(k)
	printDate(timeString, k, img)
	sendImage(w, img)
}

func getArguments(r http.Request) (k int, time string, err error) {
	q := r.URL.Query()
	kStr := q.Get("k")
	if len(kStr) < 1 {
		k = 1
	} else {
		k, err = strconv.Atoi(kStr)
		if err != nil || k < 1 || k > 30 {
			return 0, "", errors.New("invalid K")
		}
	}
	time = q.Get("time")
	time, err = formatDate(time)
	if err != nil {
		return 0, "", err
	}
	return k, time, err
}

func createImage(multiplier int) *image.RGBA {
	w = (6*8 + 2*4) * multiplier
	h = multiplier * patternHeight
	img := image.NewRGBA(image.Rect(0, 0, w, h))
	return img
}

func saveImage(img *image.RGBA) {
	f, _ := os.Create("/tmp/tame.png")
	_ = png.Encode(f, img)
}

func drawPattern(leftMargin, multiplier int, pattern string, img *image.RGBA) int {
	patternWidth := len(pattern) / patternHeight
	for i := 0; i < patternWidth*multiplier; i++ {
		for j := 0; j < patternHeight*multiplier; j++ {
			char := pattern[j/multiplier*(patternWidth+1)+i/multiplier]
			cl := White
			if char == '1' {
				cl = Cyan
			}
			img.Set(i+leftMargin*multiplier, j, cl)
		}
	}
	return leftMargin + patternWidth
}

func formatDate(inputDate string) (string, error) {
	if len(inputDate) != 8 && len(inputDate) > 0 {
		return "", errors.New("incorrect input date")
	}
	resTime := time.Now()
	if len(inputDate) == 8 {
		var err error
		resTime, err = time.Parse(timePattern, inputDate)
		if err != nil {
			return "", err
		}
	}
	return resTime.Format(timePattern), nil
}

func printDate(date string, multiplier int, img *image.RGBA) {
	margin := 0
	for _, s := range date {
		margin = drawPattern(margin, multiplier, symbolPatterns[s], img)
	}
}

func sendImage(w http.ResponseWriter, img *image.RGBA) {
	w.Header().Add("Content-Type", "image/png")
	if err := png.Encode(w, img); err != nil {
		log.Println("unable to encode image.")
	}
}

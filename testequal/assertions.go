// +build !solution

package testequal

import "fmt"

// AssertEqual checks that expected and actual are equal.
//
// Marks caller function as having failed but continues execution.
//
// Returns true iff arguments are equal.
func AssertEqual(t T, expected, actual interface{}, msgAndArgs ...interface{}) bool {
	t.Helper()
	equals := testEquals(actual, expected)
	if !equals {
		msg := fmtstr(msgAndArgs)
		t.Errorf("expected: %s\nactual: %s\nmessage: %s:", expected, actual, msg)
	}
	return equals
}

// AssertNotEqual checks that expected and actual are not equal.
//
// Marks caller function as having failed but continues execution.
//
// Returns true iff arguments are not equal.
func AssertNotEqual(t T, expected, actual interface{}, msgAndArgs ...interface{}) bool {
	t.Helper()
	equals := testEquals(actual, expected)

	if equals {
		msg := fmtstr(msgAndArgs)
		t.Errorf("expected: %s\nactual: %s\nmessage: %s:", expected, actual, msg)
	}
	return !equals
}

// RequireEqual does the same as AssertEqual but fails caller test immediately.
func RequireEqual(t T, expected, actual interface{}, msgAndArgs ...interface{}) {
	t.Helper()
	equals := testEquals(actual, expected)

	if !equals {
		msg := fmtstr(msgAndArgs)
		t.Errorf("expected: %s\nactual: %s\nmessage: %s:", expected, actual, msg)
		t.FailNow()
	}
}

// RequireNotEqual does the same as AssertNotEqual but fails caller test immediately.
func RequireNotEqual(t T, expected, actual interface{}, msgAndArgs ...interface{}) {
	t.Helper()
	equals := testEquals(actual, expected)
	if equals {
		msg := fmtstr(msgAndArgs)
		t.Errorf("expected: %s\nactual: %s\nmessage: %s:", expected, actual, msg)
		t.FailNow()
	}
}

func stringAreEquals(actual string, expected interface{}) bool {
	s, ok := expected.(string)
	if !ok {
		return false
	}
	return s == actual
}

func mapsAreEquals(actual map[string]string, expected interface{}) bool {
	m, ok := expected.(map[string]string)
	if !ok {
		return false
	}
	if m == nil || actual == nil {
		return false
	}
	if len(m) != len(actual) {
		return false
	}
	for key, value := range actual {
		value2, ok := m[key]
		if !ok {
			return false
		}
		if value2 != value {
			return false
		}
	}
	return true
}

func intArraysAreEquals(actual []int, expected interface{}) bool {
	expectItems, ok := expected.([]int)
	if !ok {
		return false
	}
	if expectItems == nil || actual == nil {
		return false
	}
	if len(expectItems) != len(actual) {
		return false
	}
	for i, item := range expectItems {
		if item != actual[i] {
			return false
		}
	}
	return true
}

func byteArraysAreEquals(actual []byte, expected interface{}) bool {
	expectItems, ok := expected.([]byte)
	if !ok {
		return false
	}
	if expectItems == nil || actual == nil {
		return false
	}
	if len(expectItems) != len(actual) {
		return false
	}
	for i, item := range expectItems {
		if item != actual[i] {
			return false
		}
	}
	return true
}

func intsAreEquals(actual, expected interface{}) bool {
	return actual == expected
}

func fmtstr(msgAndArgs ...interface{}) string {
	msgs := msgAndArgs[0].([]interface{})
	if len(msgs) == 0 {
		return ""
	}
	msg := fmt.Sprintf("%s", msgs[0])
	if len(msgs) == 1 {
		return msg
	}
	return fmt.Sprintf(msg, msgs[1:]...)
}

func testEquals(expected, actual interface{}) bool {
	switch val := actual.(type) {
	case int, uint, int8, uint8, int16, uint16, int32, uint32, int64, uint64:
		return intsAreEquals(actual, expected)
	case map[string]string:
		return mapsAreEquals(val, expected)
	case string:
		return stringAreEquals(val, expected)
	case []int:
		return intArraysAreEquals(val, expected)
	case []byte:
		return byteArraysAreEquals(val, expected)
	default:
		return false
	}
}

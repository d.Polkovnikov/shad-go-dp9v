package client

import (
	"gitlab.com/slon/shad-go/coverme/models"
	"gitlab.com/slon/shad-go/coverme/utils"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestClient_Add(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_ = utils.RespondJSON(w, http.StatusCreated, models.Todo{
			ID:       0,
			Title:    "TITLE",
			Content:  "MESSAGE",
			Finished: true,
		})
	}))
	defer ts.Close()

	client := New(ts.URL)
	todo, err := client.Add(&models.AddRequest{
		Title:   "TITLE",
		Content: "MESSAGE",
	})
	if err != nil {
		t.Fatalf("Error in success responce")
	}

	if todo.ID != 0 {
		t.Errorf("ToDo.id fail: actual %d; expected %d", todo.ID, 0)
	}
	if todo.Title != "TITLE" {
		t.Errorf("ToDo.Title fail: actual %s; expected %s", todo.Title, "TITLE")
	}
	if todo.Content != "MESSAGE" {
		t.Errorf("ToDo.Content fail: actual %s; expected %s", todo.Content, "MESSAGE")
	}
	if !todo.Finished {
		t.Errorf("ToDo.Finished fail: actual %t; expected %t", todo.Finished, true)
	}
}

func TestClient_Add_BadStatus(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_ = utils.RespondJSON(w, http.StatusBadRequest, models.Todo{})
	}))
	defer ts.Close()

	client := New(ts.URL)
	_, err := client.Add(&models.AddRequest{})
	if err == nil {
		t.Fatalf("expected error response")
	}
	if err.Error() != "unexpected status code 400" {
		t.Error("Unexpected error message")
	}
}

func TestClient_Add_BadServerResponse(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {}))
	ts.Close()

	client := New(ts.URL)
	_, err := client.Add(&models.AddRequest{})
	if err == nil {
		t.Fatalf("expected error response")
	}
}

func TestClient_Get(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_ = utils.RespondJSON(w, http.StatusOK, models.Todo{
			ID:       0,
			Title:    "TITLE",
			Content:  "MESSAGE",
			Finished: true,
		})
	}))
	defer ts.Close()

	client := New(ts.URL)
	todo, err := client.Get(0)
	if err != nil {
		t.Fatalf("Error in success responce")
	}

	if todo.ID != 0 {
		t.Errorf("ToDo.id fail: actual %d; expected %d", todo.ID, 0)
	}
	if todo.Title != "TITLE" {
		t.Errorf("ToDo.Title fail: actual %s; expected %s", todo.Title, "TITLE")
	}
	if todo.Content != "MESSAGE" {
		t.Errorf("ToDo.Content fail: actual %s; expected %s", todo.Content, "MESSAGE")
	}
	if !todo.Finished {
		t.Errorf("ToDo.Finished fail: actual %t; expected %t", todo.Finished, true)
	}
}

func TestClient_Get_BadStatus(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_ = utils.RespondJSON(w, http.StatusBadRequest, models.Todo{})
	}))
	defer ts.Close()

	client := New(ts.URL)
	_, err := client.Get(1)
	if err == nil {
		t.Fatalf("expected error response")
	}
	if err.Error() != "unexpected status code 400" {
		t.Error("Unexpected error message")
	}
}

func TestClient_Get_BadServerResponse(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {}))
	ts.Close()

	client := New(ts.URL)
	_, err := client.Get(1)
	if err == nil {
		t.Fatalf("expected error response")
	}
}

func TestClient_List(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_ = utils.RespondJSON(w, http.StatusOK, []*models.Todo{
			{
				ID:       0,
				Title:    "TITLE_0",
				Content:  "MESSAGE_0",
				Finished: true,
			},
			{
				ID:       1,
				Title:    "TITLE_1",
				Content:  "MESSAGE_1",
				Finished: false,
			},
		})
	}))
	defer ts.Close()

	client := New(ts.URL)
	todos, err := client.List()
	if err != nil {
		t.Fatalf("Error in success responce")
	}

	if len(todos) != 2 {
		t.Errorf("Expected %d items, actual: %d", len(todos), 2)
	}
}

func TestClient_List_BadStatus(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_ = utils.RespondJSON(w, http.StatusBadRequest, models.Todo{})
	}))
	defer ts.Close()

	client := New(ts.URL)
	_, err := client.List()
	if err == nil {
		t.Fatalf("expected error response")
	}
	if err.Error() != "unexpected status code 400" {
		t.Error("Unexpected error message")
	}
}

func TestClient_List_BadServerResponse(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {}))
	ts.Close()

	client := New(ts.URL)
	_, err := client.List()
	if err == nil {
		t.Fatalf("expected error response")
	}
}

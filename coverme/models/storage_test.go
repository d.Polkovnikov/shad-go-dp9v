// +build !change

package models

import "testing"

func TestInMemoryStorage_AddTodo(t *testing.T) {
	storage := NewInMemoryStorage()

	if storage.nextID != 0 {
		t.Errorf("nextId fail: actual %d; expected %d", storage.nextID, 0)
	}

	todo, err := storage.AddTodo("TITLE", "MESSAGE")

	if err != nil {
		t.Fatalf("AddTodo return error")
	}
	if todo.ID != 0 {
		t.Errorf("ToDo.id fail: actual %d; expected %d", todo.ID, 0)
	}
	if todo.Title != "TITLE" {
		t.Errorf("ToDo.Title fail: actual %s; expected %s", todo.Title, "TITLE")
	}
	if todo.Content != "MESSAGE" {
		t.Errorf("ToDo.Content fail: actual %s; expected %s", todo.Content, "MESSAGE")
	}
	if todo.Finished {
		t.Errorf("ToDo.Finished fail: actual %t; expected %t", todo.Finished, false)
	}
	if len(storage.todos) != 1 {
		t.Errorf("strorage len fail: actual %d; expected %d", len(storage.todos), 1)
	}
	if storage.nextID != 1 {
		t.Errorf("nextId fail: actual %d; expected %d", storage.nextID, 1)
	}
}

func TestInMemoryStorage_GetTodo_Exists(t *testing.T) {
	storage := NewInMemoryStorage()
	_, _ = storage.AddTodo("TITLE", "MESSAGE")
	todo, err := storage.GetTodo(0)

	if err != nil {
		t.Fatalf("GetTodo return error")
	}
	if todo.Title != "TITLE" {
		t.Errorf("ToDo.Title fail: actual %s; expected %s", todo.Title, "TITLE")
	}
	if todo.Content != "MESSAGE" {
		t.Errorf("ToDo.Content fail: actual %s; expected %s", todo.Content, "MESSAGE")
	}
}

func TestInMemoryStorage_GetTodo_NotExists(t *testing.T) {
	storage := NewInMemoryStorage()
	_, err := storage.GetTodo(0)
	if err == nil {
		t.Fatalf(" Expecct GetTodo return error")
	}
}

func TestInMemoryStorage_GetAll(t *testing.T) {
	storage := NewInMemoryStorage()
	_, _ = storage.AddTodo("TITLE1", "MESSAGE1")
	_, _ = storage.AddTodo("TITLE2", "MESSAGE2")
	_, _ = storage.AddTodo("TITLE3", "MESSAGE3")
	todos, err := storage.GetAll()
	if err != nil {
		t.Fatalf("GetTodo return error")
	}
	if len(todos) != 3 {
		t.Fatalf("GetAll return %d, expected: %d", len(todos), 3)
	}
}

func TestTodo_MarkFinished_MarkUnfinished(t *testing.T) {
	storage := NewInMemoryStorage()
	_, _ = storage.AddTodo("TITLE", "MESSAGE")
	todo, _ := storage.GetTodo(0)
	todo.MarkFinished()
	todo, _ = storage.GetTodo(0)
	if !todo.Finished {
		t.Fatalf("Expected ToDo.Finished == true, but false")
	}
	todo.MarkUnfinished()
	todo, _ = storage.GetTodo(0)
	if todo.Finished {
		t.Fatalf("Expected ToDo.Finished == false, but true")
	}
}

// +build !change

package app

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/golang/mock/gomock"
	"gitlab.com/slon/shad-go/coverme/client"
	"gitlab.com/slon/shad-go/coverme/mock_models"
	"gitlab.com/slon/shad-go/coverme/models"
	"net/http"
	"net/http/httptest"
	"testing"
)

// min coverage: 90%

func TestPing(t *testing.T) {
	rootPath := createTestServer(t)
	resp, _ := http.Get(rootPath)
	if resp.StatusCode != 200 {
		t.Error("/ping not return 200 code")
	}
}

func TestApp_addToDo(t *testing.T) {
	rootPath := createTestServer(t)
	client := client.New(rootPath)
	addTodo, err := client.Add(&models.AddRequest{
		Title:   "RES_TITLE",
		Content: "RES_CONTENT",
	})
	if err != nil {
		t.Fatalf("error: %s", err)
	}
	if addTodo.ID != 100 {
		t.Errorf("ToDo.Id fail: actual %d; expected %d", addTodo.ID, 100)
	}
	if addTodo.Title != "RES_TITLE" {
		t.Errorf("ToDo.Title fail: actual %s; expected %s", addTodo.Title, "RES_TITLE")
	}
	if addTodo.Content != "RES_CONTENT" {
		t.Errorf("ToDo.Content fail: actual %s; expected %s", addTodo.Content, "RES_CONTENT")
	}
}

func TestApp_addToDo_BadJson(t *testing.T) {
	rootPath := createTestServer(t)
	data := []byte{'{', 'x', ':', '}'}
	resp, err := http.Post(rootPath+"/todo/create", "application/json", bytes.NewReader(data))
	if err != nil {
		panic(err)
	}
	if resp.StatusCode != 400 {
		t.Errorf("BadRequest(400) expected, actual:%d", resp.StatusCode)
	}
}

func TestApp_addToDo_NoTitle(t *testing.T) {
	rootPath := createTestServer(t)
	data, _ := json.Marshal(models.AddRequest{
		Content: "RES_CONTENT",
	})
	resp, err := http.Post(rootPath+"/todo/create", "application/json", bytes.NewReader(data))
	if err != nil {
		panic(err)
	}
	if resp.StatusCode != 400 {
		t.Errorf("BadRequest(400) expected, actual:%d", resp.StatusCode)
	}
}

func TestApp_addToDo_StorageError(t *testing.T) {
	rootPath := createTestServer(t)
	data, _ := json.Marshal(models.AddRequest{
		Title:   "BAD_TITLE",
		Content: "BAD_CONTENT",
	})
	resp, err := http.Post(rootPath+"/todo/create", "application/json", bytes.NewReader(data))
	if err != nil {
		panic(err)
	}
	if resp.StatusCode != 500 {
		t.Errorf("ServerError(500) expected, actual:%d", resp.StatusCode)
	}
}

func TestApp_getToDoById_BadId(t *testing.T) {
	rootPath := createTestServer(t)
	resp, err := http.Get(rootPath + fmt.Sprintf("/todo/%s", "999999999999999999999"))
	if err != nil {
		t.Fatalf("error: %s", err)
	}
	if resp.StatusCode != 400 {
		t.Errorf("BadRequest(400) expected, actual:%d", resp.StatusCode)
	}
}

func TestApp_getToDoById_NoId(t *testing.T) {
	rootPath := createTestServer(t)
	resp, err := http.Get(rootPath + fmt.Sprintf("/todo/%d", 200))
	if err != nil {
		t.Fatalf("error: %s", err)
	}
	if resp.StatusCode != 500 {
		t.Errorf("ServerError(500) expected, actual:%d", resp.StatusCode)
	}
}

func TestApp_getToDoById(t *testing.T) {
	rootPath := createTestServer(t)
	client := client.New(rootPath)
	todo, err := client.Get(100)
	if err != nil {
		t.Fatalf("error: %s", err)
	}
	if todo.ID != 100 {
		t.Errorf("ToDo.Id fail: actual %d; expected %d", todo.ID, 100)
	}
	if todo.Title != "RES_TITLE" {
		t.Errorf("ToDo.Title fail: actual %s; expected %s", todo.Title, "RES_TITLE")
	}
	if todo.Content != "RES_CONTENT" {
		t.Errorf("ToDo.Content fail: actual %s; expected %s", todo.Content, "RES_CONTENT")
	}
}

func TestApp_getAll(t *testing.T) {
	rootPath := createTestServer(t)
	client := client.New(rootPath)
	list, err := client.List()
	if err != nil {
		t.Fatalf("error: %s", err)
	}
	if len(list) != 2 {
		t.Errorf("List retunr %d items, expected: %d", len(list), 2)
	}
	resp, err := http.Get(rootPath + "/todo")
	if err != nil {
		t.Fatalf("error: %s", err)
	}
	if resp.StatusCode != 500 {
		t.Errorf("ServerError(500) expected, actual:%d", resp.StatusCode)
	}
}

func createTestServer(t *testing.T) string {
	ctrl := gomock.NewController(t)
	storage := mock_models.NewMockStorage(ctrl)
	gomock.InOrder(
		storage.EXPECT().
			AddTodo(gomock.Eq("RES_TITLE"), gomock.Eq("RES_CONTENT")).
			Return(&models.Todo{
				ID:       100,
				Title:    "RES_TITLE",
				Content:  "RES_CONTENT",
				Finished: false,
			}, nil).
			AnyTimes(),
		storage.EXPECT().
			AddTodo(gomock.Eq("BAD_TITLE"), gomock.Eq("BAD_CONTENT")).
			Return(nil, errors.New("err")).
			AnyTimes(),

		storage.EXPECT().
			GetTodo(models.ID(100)).
			Return(&models.Todo{
				ID:       100,
				Title:    "RES_TITLE",
				Content:  "RES_CONTENT",
				Finished: false,
			}, nil).
			AnyTimes(),

		storage.EXPECT().
			GetTodo(models.ID(200)).
			Return(nil, fmt.Errorf("todo 200 not found")).
			AnyTimes(),

		storage.EXPECT().
			GetAll().
			Return([]*models.Todo{
				{
					ID:       100,
					Title:    "RES_TITLE_1",
					Content:  "RES_CONTENt_2",
					Finished: false,
				},
				{
					ID:       101,
					Title:    "RES_TITLE_2",
					Content:  "RES_CONTENT_2",
					Finished: true,
				},
			}, nil).
			Times(1),

		storage.EXPECT().
			GetAll().
			Return([]*models.Todo{}, errors.New("err")).
			AnyTimes(),
	)
	app := New(storage)
	app.initRoutes()
	server := httptest.NewServer(app.router)
	client.New(server.URL)
	return server.URL
}

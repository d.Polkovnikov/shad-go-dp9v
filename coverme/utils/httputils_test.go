package utils

import (
	"github.com/stretchr/testify/http"
	"testing"
)

func TestRespondJSON(t *testing.T) {

	res := http.TestResponseWriter{
		StatusCode: 0,
		Output:     "",
	}
	_ = RespondJSON(&res, 200, struct {
		Res int
	}{
		Res: 1,
	})

	respType := res.Header().Get("Content-Type")
	expectedResponse := "{\"Res\":1}"

	if respType != "application/json" {
		t.Errorf("Bad response type. Expected: %s. Actual: %s", "application/json", respType)
	}

	if res.Output != expectedResponse {
		t.Errorf("Bad output. Expected: %s. Actual: %s", expectedResponse, res.Output)
	}

	if res.StatusCode != 200 {
		t.Errorf("Bad status code. Expected: %d. Actual: %d", 200, res.StatusCode)
	}
}

func TestBadRequest(t *testing.T) {
	res := http.TestResponseWriter{
		StatusCode: 0,
		Output:     "",
	}

	BadRequest(&res, "test")

	if res.StatusCode != 400 {
		t.Errorf("Bad status code. Expected: %d. Actual: %d", 400, res.StatusCode)
	}

	if res.Output != "test" {
		t.Errorf("Bad output. Expected: %s. Actual: %s", "test", res.Output)
	}
}

func TestServerError(t *testing.T) {
	res := http.TestResponseWriter{
		StatusCode: 0,
		Output:     "",
	}

	ServerError(&res)

	if res.StatusCode != 500 {
		t.Errorf("Bad status code. Expected: %d. Actual: %d", 400, res.StatusCode)
	}

	if res.Output != "Server encountered an error." {
		t.Errorf("Bad output. Expected: %s. Actual: %s", "Server encountered an error.", res.Output)
	}
}

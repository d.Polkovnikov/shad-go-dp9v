// +build !solution

package batcher

import (
	"gitlab.com/slon/shad-go/batcher/slow"
	"sync"
)

type Batcher struct {
	valueLock sync.Mutex
	flagLock  sync.Mutex
	locker    chan bool
	isLoad    bool
	value     interface{}
	v         *slow.Value
}

func NewBatcher(v *slow.Value) *Batcher {
	return &Batcher{
		valueLock: sync.Mutex{},
		flagLock:  sync.Mutex{},
		locker:    make(chan bool),
		isLoad:    false,
		v:         v,
	}
}

func (b *Batcher) Load() interface{} {
	b.flagLock.Lock()
	if b.isLoad {
		b.flagLock.Unlock()
		<-b.locker
	} else {
		b.isLoad = true
		b.flagLock.Unlock()
		b.updateValue()
	}
	return b.getValue()
}

func (b *Batcher) updateValue() {
	b.valueLock.Lock()
	b.value = b.v.Load()
	b.valueLock.Unlock()
	b.isLoad = false
	close(b.locker)
	b.locker = make(chan bool)
}

func (b *Batcher) getValue() interface{} {
	b.valueLock.Lock()
	defer b.valueLock.Unlock()
	return b.value
}

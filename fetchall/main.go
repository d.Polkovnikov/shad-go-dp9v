// +build !solution

package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"sync"
	"time"
)

func main() {
	startTime := time.Now()
	var wg sync.WaitGroup
	urls := getUrlsFromArgs()
	urlCount := len(urls)
	wg.Add(urlCount)
	for _, value := range urls {
		go func(url string) {
			defer wg.Done()
			loadURL(url)
		}(value)
	}
	wg.Wait()
	duration := time.Since(startTime)
	fmt.Printf("%s\n", duration)
}

func getUrlsFromArgs() []string {
	return os.Args[1:]
}

func loadURL(url string) {
	startTime := time.Now()
	response, err := http.Get(url)
	if err != nil {
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return
	}
	prettyPrint(string(len(body)), url, startTime)
}

func prettyPrint(result string, url string, startTime time.Time) {
	duration := time.Since(startTime)
	fmt.Printf("%s\t%s\t%s\n", duration, result, url)
}

// +build !solution

package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

func main() {
	args := getUrlsFromArgs()
	for _, arg := range args {
		body, err := loadURL(arg)
		if err != nil {
			fmt.Printf("error: %v\n", err)
			os.Exit(1)
		}
		fmt.Printf("%s", body)
	}
}

func getUrlsFromArgs() []string {
	return os.Args[1:]
}

func loadURL(url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	return string(body), nil
}

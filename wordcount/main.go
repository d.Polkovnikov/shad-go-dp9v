// +build !solution

package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func main() {
	argsWithoutProg := os.Args[1:]
	res := make(map[string]int)
	for _, val := range argsWithoutProg {
		dat, _ := ioutil.ReadFile(val)
		words := strings.Split(string(dat), "\n")
		for _, word := range words {
			_, ok := res[word]
			if ok {
				res[word]++
			} else {
				res[word] = 1
			}
		}
	}

	for word, count := range res {
		if count > 1 {
			fmt.Printf("%d\t%s\n", count, word)
		}
	}
}

package main

import (
	"flag"
)

var (
	addr = flag.String("addr", "", "")
)

func main() {
	flag.Parse()
	StartSocketClient(*addr)
}

package main

import (
	"bufio"
	context2 "context"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"os"
	"os/signal"
	"syscall"
)

type Client struct {
	ws              *websocket.Conn
	readMessageChan chan string
	sendMessageChan chan string
	context         context2.Context
}

func StartSocketClient(wsURL string) {
	closeSig := make(chan os.Signal, 1)
	signal.Notify(closeSig, syscall.SIGINT, syscall.SIGTERM)
	mainContext, cancelFunc := context2.WithCancel(context2.Background())
	defer cancelFunc()
	connection, _, err := websocket.DefaultDialer.DialContext(mainContext, wsURL, nil)
	if err != nil {
		return
	}

	newClient := &Client{
		ws:              connection,
		readMessageChan: make(chan string),
		sendMessageChan: make(chan string),
		context:         mainContext,
	}

	go newClient.listenStdio()
	go newClient.listenSocket()
	for {
		select {
		case <-closeSig:
			return
		case msg := <-newClient.readMessageChan:
			_, _ = fmt.Fprint(os.Stdout, msg)
		case msg := <-newClient.sendMessageChan:
			_ = connection.WriteMessage(websocket.TextMessage, []byte(msg))
		}
	}
}

func (c *Client) listenStdio() {
	reader := bufio.NewScanner(os.Stdin)
	for {
		select {
		case <-c.context.Done():
			return
		default:
			if reader.Scan() {
				c.sendMessageChan <- reader.Text()
			}
		}
	}
}

func (c *Client) listenSocket() {
	for {
		select {
		case <-c.context.Done():
			return
		default:
			_, msg, err := c.ws.ReadMessage()
			if err != nil {
				log.Println("read:", err)
			}
			c.readMessageChan <- string(msg)
		}
	}
}

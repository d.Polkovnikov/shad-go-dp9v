package jsonrpc

import (
	"encoding/json"
	"net/http"
	"reflect"
)

type MyHandler struct {
	s              interface{}
	requestParams  map[string]reflect.Type
	responseParams map[string]reflect.Type
}

func (m MyHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	methodName := r.URL.Path[1:]

	requestType, ok := m.requestParams[methodName]
	if !ok {
		http.NotFound(w, r)
		return
	}

	decoder := json.NewDecoder(r.Body)
	value := reflect.New(requestType)
	err := decoder.Decode(value.Interface())

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	reqValues := make([]reflect.Value, 2)
	reqValues[0] = reflect.ValueOf(r.Context())
	reqValues[1] = value
	respValues := reflect.ValueOf(m.s).MethodByName(methodName).Call(reqValues)

	if !respValues[1].IsNil() {
		err = respValues[1].Interface().(error)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	encoder := json.NewEncoder(w)
	err = encoder.Encode(respValues[0].Interface())
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}

func getMethodToRequest(s interface{}) map[string]reflect.Type {
	methods := make(map[string]reflect.Type)
	t := reflect.TypeOf(s)
	for i := 0; i < t.NumMethod(); i++ {
		method := t.Method(i)
		methodType := method.Type
		inValue := methodType.In(2)
		methods[method.Name] = inValue.Elem()
	}
	return methods
}
func getMethodToResponse(s interface{}) map[string]reflect.Type {
	methods := make(map[string]reflect.Type)
	t := reflect.TypeOf(s)
	for i := 0; i < t.NumMethod(); i++ {
		method := t.Method(i)
		methodType := method.Type
		inValue := methodType.Out(0)
		methods[method.Name] = inValue.Elem()
	}
	return methods
}

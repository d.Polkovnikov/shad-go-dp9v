// +build !solution

package jsonrpc

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
)

func MakeHandler(service interface{}) http.Handler {
	request := getMethodToRequest(service)
	response := getMethodToResponse(service)
	return MyHandler{
		s:              service,
		requestParams:  request,
		responseParams: response,
	}
}

func Call(ctx context.Context, endpoint string, method string, req, rsp interface{}) error {

	buffer := bytes.NewBuffer(make([]byte, 0))
	encoder := json.NewEncoder(buffer)
	err := encoder.Encode(req)
	if err != nil {
		return err
	}

	path := fmt.Sprintf("%s/%s", endpoint, method)
	request, err := http.NewRequestWithContext(ctx, http.MethodPost, path, buffer)
	if err != nil {
		return err
	}
	client := http.Client{}
	response, err := client.Do(request)
	if err != nil {
		return err
	}

	if response.StatusCode == http.StatusInternalServerError {
		errMessage, _ := ioutil.ReadAll(response.Body)
		return errors.New(string(errMessage))
	}

	decoder := json.NewDecoder(response.Body)
	return decoder.Decode(rsp)
}

// +build !solution

package varfmt

import (
	"fmt"
	"strconv"
	"strings"
)

func Sprintf(format string, args ...interface{}) string {
	items := make([]string, len(args))
	for i, val := range args {
		items[i] = fmt.Sprint(val)
	}
	builder := strings.Builder{}
	builder.Grow(len(format))

	parts := strings.Split(format, "{")
	for i, part := range parts {
		index := i - 1
		if i != 0 {
			indexOf := strings.Index(part, "}")
			if indexOf != 0 {
				index, _ = strconv.Atoi(part[:indexOf])
			}
			builder.WriteString(items[index])
			builder.WriteString(part[indexOf+1:])
		} else {
			builder.WriteString(part)
		}
	}
	return builder.String()
}

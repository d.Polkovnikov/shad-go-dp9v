package main

import (
	"context"
	"fmt"
)

func main() {
}

type testService struct{}

type PingRequest struct{}
type PingResponse struct{}

func (*testService) Ping(ctx context.Context, req *PingRequest) (*PingResponse, error) {
	return &PingResponse{}, nil
}

type AddRequest struct{ A, B int }
type AddResponse struct{ Sum int }

func (*testService) Add(ctx context.Context, req *AddRequest) (*AddResponse, error) {
	return &AddResponse{Sum: req.A + req.B}, nil
}

type ErrorRequest struct{}
type ErrorResponse struct{}

func (*testService) Error(ctx context.Context, req *ErrorRequest) (*ErrorResponse, error) {
	return nil, fmt.Errorf("cache is empty")
}

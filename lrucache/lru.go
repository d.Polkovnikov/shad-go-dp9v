// +build !solution

package lrucache

import (
	"container/list"
)

func New(cap int) Cache {
	return lruCache{
		maxSize: cap,
		values:  make(map[int]*list.Element),
		queue:   list.New(),
	}
}

type KeyValue struct {
	key   int
	value int
}

type lruCache struct {
	maxSize int
	values  map[int]*list.Element
	queue   *list.List
}

func (c lruCache) Set(key, value int) {
	if c.maxSize < 1 {
		return
	}
	node, ok := c.values[key]
	if ok {
		c.queue.Remove(node)
	} else {
		if c.queue.Len() == c.maxSize {
			nodeToRemove := c.queue.Back()
			delete(c.values, nodeToRemove.Value.(KeyValue).key)
			c.queue.Remove(nodeToRemove)
		}
	}
	node = c.queue.PushFront(KeyValue{key: key, value: value})
	c.values[key] = node
}

func (c lruCache) Get(key int) (int, bool) {
	node, ok := c.values[key]
	if ok {
		return node.Value.(KeyValue).value, true
	}
	return 0, false
}

func (c lruCache) Range(f func(key, value int) bool) {
	for node := c.queue.Back(); node != nil; node = node.Prev() {
		value := node.Value.(KeyValue)
		if !f(value.key, value.value) {
			return
		}
	}
}

func (c lruCache) Clear() {
	c.queue.Init()
	for key := range c.values {
		delete(c.values, key)
	}
}

// +build !solution

package retryupdate

import (
	"errors"
	"github.com/gofrs/uuid"
	"gitlab.com/slon/shad-go/retryupdate/kvapi"
)

var (
	authErr     *kvapi.AuthError
	conflictErr *kvapi.ConflictError
	apiErr      *kvapi.APIError
	retryErr    *RetryError
)

func UpdateValue(c kvapi.Client, key string, updateFn func(oldValue *string) (newValue string, err error)) error {
outerloop:
	for {
		response, err := loadGetResponse(c, key)
		if err != nil {
			return err
		}

	innerLoop:
		for {
			newValue, err := getNewValue(response, updateFn)
			if err != nil {
				return err
			}

			oldUUID := uuid.Nil
			if response != nil {
				oldUUID = response.Version
			}

			err = setNewValue(c, key, newValue, oldUUID)
			if errors.As(err, &retryErr) {
				continue outerloop
			}
			if errors.Is(err, kvapi.ErrKeyNotFound) {
				response = nil
				continue innerLoop
			}
			return err
		}
	}
}

type RetryError struct {
	Err error
}

func (a *RetryError) Error() string {
	return a.Err.Error()
}

func (a *RetryError) Unwrap() error {
	return a.Err
}

func loadGetResponse(c kvapi.Client, key string) (*kvapi.GetResponse, error) {
	for {
		response, err := c.Get(&kvapi.GetRequest{Key: key})
		if errors.Is(err, kvapi.ErrKeyNotFound) {
			return nil, nil
		} else if errors.As(err, &authErr) {
			return nil, err
		} else if err == nil {
			return response, nil
		}
	}
}

func getNewValue(response *kvapi.GetResponse, updateFn func(oldValue *string) (newValue string, err error)) (newValue string, err error) {
	if response == nil {
		newValue, err = updateFn(nil)
	} else {
		newValue, err = updateFn(&response.Value)
	}
	if err != nil {
		return "", err
	}
	return newValue, err
}

func setNewValue(c kvapi.Client, key, newValue string, oldUUID uuid.UUID) error {
	prevUUID := uuid.Nil
	for {
		setRequest := kvapi.SetRequest{
			Key:        key,
			Value:      newValue,
			OldVersion: oldUUID,
			NewVersion: uuid.Must(uuid.NewV4()),
		}
		_, err := c.Set(&setRequest)

		if err == nil {
			return nil
		}
		if errors.As(err, &conflictErr) {
			expectedVersion := errors.Unwrap(err).(*kvapi.ConflictError).ExpectedVersion
			if expectedVersion == prevUUID {
				return nil
			}
			return &RetryError{Err: err}
		} else if errors.As(err, &authErr) {
			return err
		} else if errors.Is(err, kvapi.ErrKeyNotFound) {
			return err
		}
		prevUUID = setRequest.NewVersion
	}
}

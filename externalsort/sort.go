// +build !solution

package externalsort

import (
	"bufio"
	"container/heap"
	"errors"
	"io"
	"os"
	"sort"
)

func NewReader(r io.Reader) LineReader {
	return CustomReader{reader: bufio.NewReader(r)}
}

func NewWriter(w io.Writer) LineWriter {
	return CustomWriter{writer: bufio.NewWriter(w)}
}

func Merge(w LineWriter, readers ...LineReader) error {
	pq := make(PriorityQueue, 0)
	for i, reader := range readers {
		line, err := reader.ReadLine()
		if (err != nil && !errors.Is(err, io.EOF)) || (errors.Is(err, io.EOF) && line == "") {
			continue
		}
		value := KeyValue{
			number: i,
			value:  line,
			index:  pq.Len(),
		}
		pq = append(pq, &value)
	}
	heap.Init(&pq)

	for pq.Len() > 0 {
		item := heap.Pop(&pq).(*KeyValue)
		_ = w.Write(item.value)
		line, err := readers[item.number].ReadLine()
		if err != io.EOF || len(line) > 0 {
			newValue := KeyValue{
				number: item.number,
				value:  line,
			}
			heap.Push(&pq, &newValue)
		}
	}
	return nil
}

func Sort(w io.Writer, in ...string) error {
	newFiles := make([]LineReader, len(in))
	for i, fName := range in {
		newFName, err := sortStringsInFile(fName)
		if err != nil {
			return err
		}
		file, err := os.Open(newFName)
		if err != nil {
			return err
		}
		newFiles[i] = NewReader(file)
	}
	writer := NewWriter(w)
	err := Merge(writer, newFiles...)
	if err != nil {
		return err
	}
	return nil
}

func readLines(fName string) (lines Lines, err error) {
	file, err := os.Open(fName)
	defer func() { _ = file.Close() }()
	if err != nil {
		return nil, err
	}
	r := NewReader(file)
	for {
		l, err := r.ReadLine()
		if err != nil {
			if errors.Is(err, io.EOF) {
				if l != "" {
					lines = append(lines, l)
				}
				return lines, nil
			}
			return nil, err
		}
		lines = append(lines, l)
	}
}

func sortStringsInFile(fName string) (string, error) {
	lines, err := readLines(fName)
	if err != nil {
		return "", err
	}
	sort.Sort(lines)

	newFName := fName + "_tmp"
	file, err := os.Create(newFName)
	w := NewWriter(file)
	if err != nil {
		return "", err
	}
	for _, line := range lines {
		_ = w.Write(line)
	}
	_ = file.Close()
	return newFName, nil
}

type CustomReader struct {
	reader *bufio.Reader
}

type CustomWriter struct {
	writer *bufio.Writer
}

func (r CustomReader) ReadLine() (string, error) {
	readString, err := r.reader.ReadString('\n')
	strLen := len(readString)
	if strLen > 0 && readString[strLen-1] == '\n' {
		readString = readString[0 : strLen-1]
	}
	return readString, err
}

func (w CustomWriter) Write(line string) error {
	_, err := w.writer.WriteString(line)
	if err != nil {
		return err
	}
	_, err = w.writer.WriteRune('\n')
	if err != nil {
		return err
	}
	return w.writer.Flush()
}

type KeyValue struct {
	number int
	value  string
	index  int
}

type PriorityQueue []*KeyValue

func (p PriorityQueue) Len() int {
	return len(p)
}

func (p PriorityQueue) Less(i, j int) bool {
	return p[i].value < p[j].value
}

func (p PriorityQueue) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
	p[i].index = j
	p[j].index = i
}

func (p *PriorityQueue) Push(x interface{}) {
	n := p.Len()
	item := x.(*KeyValue)
	item.index = n
	*p = append(*p, item)
}

func (p *PriorityQueue) Pop() interface{} {
	old := *p
	n := len(old)
	item := old[n-1]
	old[n-1] = nil
	item.index = -1
	*p = old[0 : n-1]
	return item
}

type Lines []string

func (l Lines) Len() int {
	return len(l)
}

func (l Lines) Less(i, j int) bool {
	return l[i] < l[j]
}

func (l Lines) Swap(i, j int) {
	l[i], l[j] = l[j], l[i]
}

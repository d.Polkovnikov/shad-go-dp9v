// +build !solution

package hogwarts

func GetCourseList(prereqs map[string][]string) []string {
	graph := Graph{
		allNodes:     make(map[string]*Node),
		startedNodes: make(map[*Node]struct{}),
		finishNodes:  make(map[*Node]struct{}),
	}
	graph.fillNodes(prereqs)
	list := graph.getOrderedList()
	result := make([]string, len(list))
	for i, value := range list {
		result[i] = value.Name
	}
	return result
}

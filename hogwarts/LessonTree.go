package hogwarts

type Node struct {
	Name      string
	isLearned bool
	parents   []*Node
	children  []*Node
}

type Graph struct {
	allNodes     map[string]*Node
	startedNodes map[*Node]struct{}
	finishNodes  map[*Node]struct{}
}

func (g Graph) fillNodes(prereqs map[string][]string) {
	for key, cources := range prereqs {
		currentNode := g.getNode(key)
		if len(cources) > 0 {
			delete(g.startedNodes, currentNode)
		}
		for _, cource := range cources {
			parentNode := g.getNode(cource)
			parentNode.children = append(parentNode.children, currentNode)
			currentNode.parents = append(currentNode.parents, parentNode)
		}
	}
	if len(g.allNodes) != 0 && len(g.startedNodes) == 0 {
		panic("panic")
	}
}

func (g Graph) getNode(name string) *Node {
	node, ok := g.allNodes[name]
	if !ok {
		node = &Node{
			Name:      name,
			isLearned: false,
		}
		g.allNodes[name] = node
		g.startedNodes[node] = struct{}{}
	}
	return node
}

func (g Graph) getOrderedList() []*Node {
	result := make([]*Node, 0)
	nodes := make([]*Node, 0)
	for node := range g.startedNodes {
		nodes = append(nodes, node)
	}
	for i := 0; i < len(nodes); i++ {
		currentNode := nodes[i]
		if currentNode.isLearned {
			continue
		}
		if currentNode.allParentsLearned() {
			currentNode.isLearned = true
			result = append(result, currentNode)
			nodes = append(nodes, currentNode.children...)
		}
	}
	if len(result) != len(g.allNodes) {
		panic("panic")
	}
	return result
}

func (n Node) allParentsLearned() bool {
	for _, parent := range n.parents {
		if !parent.isLearned {
			return false
		}
	}
	return true
}

func (n Node) anyChildLearned() bool {
	for _, child := range n.children {
		if child.isLearned {
			return true
		}
	}
	return false
}

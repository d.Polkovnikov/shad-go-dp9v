// +build !solution

package reversemap

import (
	"reflect"
)

func ReverseMap(forward interface{}) interface{} {
	val := reflect.ValueOf(forward)
	var keyType, valType reflect.Type
	for _, value := range val.MapKeys() {
		currKeyType := value.Type()
		currValType := val.MapIndex(value).Type()
		if keyType != nil && keyType != currKeyType || valType != nil && valType != currValType {
			panic("PAAAANIIIIIKK!")
		}
		keyType = currKeyType
		valType = currValType
	}
	newMapProto := reflect.MapOf(valType, keyType)
	newMap := reflect.MakeMapWithSize(newMapProto, 3)

	for _, key := range val.MapKeys() {
		value := val.MapIndex(key)
		newMap.SetMapIndex(value, key)
	}
	return newMap.Interface()
}
